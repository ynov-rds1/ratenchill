# RateNChill (API)

Web service allowing authenticated user to share with his / her friends a list of TV Shows and Movies he / she likes.

# Technical references

- Framework : .NET Core
- Unit testing : xUnit
- Languages : C#
- Workflow : none
- External services : TMDB
- Database : SQL Server
