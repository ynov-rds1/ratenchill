using System;
using RateNChill.Entities;
using RateNChill.Entities.Medias;

namespace RateNChill.UnitTests.Utils
{
    public static class ObjectGenerators
    {
        private static readonly Random _rand = new();


        public static Media CreateRandomMedia(string title = null)
        {
            return new()
            {
                id = _rand.Next(10000),
                original_title = title == null ? "Random title - " + Guid.NewGuid().ToString() : title,
                poster_path = "RandomPath/" + _rand.Next(1000)
            };
        }

        public static User CreateRandomUser(int invBy = 0)
        {
            return new()
            {
                id = _rand.Next(10000),
                name = "Random name - " + Guid.NewGuid().ToString(),
                firstname = "Random firstname - " + Guid.NewGuid().ToString(),
                username = "Random username - " + Guid.NewGuid().ToString(),
                mail = "sand.dossantos@live.fr",
                shaPass = "Random shaPass - " + Guid.NewGuid().ToString(),
                invitedBy = invBy
            };
        }
    }
}
