using Xunit;
using RateNChill.Repositories;
using FluentAssertions;

namespace RateNChill.UnitTests.Repositories;

public class MovieDbRepositoryTests
{
    [Fact]
    public void FormatUrl_WithMediaTypeMovieAndExistingName_ReturnsUrlWithQuery()
    {
        // Arrange
        string expectedUrl =  "/search/movie?" + MovieDbRepository.API_KEY + "&query=testQuery&language=en-US&sort_by=popularity.desc&page=1";
        
        // Act
        string result = MovieDbRepository.FormatUrl(1, "testQuery");

        // Assert
        result.Should().Be(expectedUrl);
    }

    [Fact]
    public void FormatUrl_WithMediaTypeTVAndExistingName_ReturnsUrlWithQuery()
    {
        // Arrange
        string expectedUrl =  "/search/tv?" + MovieDbRepository.API_KEY + "&query=testQuery&language=en-US&sort_by=popularity.desc&page=1";
        
        // Act
        string result = MovieDbRepository.FormatUrl(2, "testQuery");

        // Assert
        result.Should().Be(expectedUrl);
    }

    [Fact]
    public void FormatUrl_WithMediaTypeAnyAndEmptyOrWhitespaceName_ReturnsUrlWithDiscover()
    {
        // Arrange
        string expectedUrl =  "/discover/movie?" + MovieDbRepository.API_KEY + "&language=en-US&sort_by=popularity.desc&page=1";
        
        // Act
        string result = MovieDbRepository.FormatUrl(1, " ");

        // Assert
        result.Should().Be(expectedUrl);
    }
    [Fact]
    public void FormatUrl_WithMediaTypeAnyAndGenreAny_ReturnsUrlWithGenre()
    {
        // Arrange
        string expectedUrl =  "/discover/movie?" + MovieDbRepository.API_KEY + "&language=en-US&sort_by=popularity.desc&page=1&with_genres=100000";
        
        // Act
        string result = MovieDbRepository.FormatUrl(1, 100000);

        // Assert
        result.Should().Be(expectedUrl);
    }
}