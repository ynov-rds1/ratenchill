using Xunit;
using FluentAssertions;
using RateNChill.Exceptions;

namespace RateNChill.UnitTests.Exceptions;

public class RegisterExceptionTests
{
    [Fact]
    public void GetMessage_WithCodeMinusTwo_ReturnsUsernameErrorMessage()
    {
        // Arrange
        string expectedMessage = "Username already taken";

        // Act
        string result = RegisterException.GetMessage(-2);

        // Assert
        result.Should().Be(expectedMessage);
    }

    [Fact]
    public void GetMessage_WithCodeMinusOne_ReturnsMailErrorMessage()
    {
        // Arrange
        string expectedMessage = "Mail address already used";

        // Act
        string result = RegisterException.GetMessage(-1);

        // Assert
        result.Should().Be(expectedMessage);
    }

    [Fact]
    public void GetMessage_WithCode0_ReturnsDefaultMessageWith0()
    {
        // Arrange
        string expectedMessage = "Something went wrong with the code 0 !";

        // Act
        string result = RegisterException.GetMessage(0);

        // Assert
        result.Should().Be(expectedMessage);
    }

    [Fact]
    public void GetMessage_WithCodeMinus5_ReturnsDefaultMessageWithMinus5()
    {
        // Arrange
        string expectedMessage = "Something went wrong with the code -5 !";

        // Act
        string result = RegisterException.GetMessage(-5);

        // Assert
        result.Should().Be(expectedMessage);
    }
}