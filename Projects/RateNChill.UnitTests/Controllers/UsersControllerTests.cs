using System.Collections.Generic;
using System.Threading.Tasks;
using FluentAssertions;
using Moq;
using RateNChill.Controllers;
using RateNChill.Dtos.Requests;
using RateNChill.Dtos.Responses;
using RateNChill.Entities;
using RateNChill.Repositories;
using Xunit;
using Microsoft.AspNetCore.Mvc;
using static RateNChill.UnitTests.Utils.ObjectGenerators;
using RateNChill.Exceptions;
using Microsoft.Extensions.Configuration;

namespace RateNChill.UnitTests.Controllers;

public class UsersControllerTests
{
    private readonly Mock<IConfiguration> _configStub = new Mock<IConfiguration>();
    private readonly Mock<IUsersRepository> _repoStub = new Mock<IUsersRepository>();

    [Fact]
    public async Task GetUserAsync_WithExistingId_ReturnsOkExpectedUserDto()
    {
        // Arrange
        var expectedUser = CreateRandomUser();
        _repoStub.Setup(repo => repo.GetUserAsync(1))
            .ReturnsAsync(expectedUser);

        var controller = new UsersController(_configStub.Object, _repoStub.Object);

        // Act
        var actionResult = await controller.GetUserAsync(1);

        // Assert
        var okResult = actionResult.Result as OkObjectResult;
        okResult.Value.Should().BeOfType<UserDto>();
    }

    [Fact]
    public async Task GetUserAsync_WithNonExistingId_ReturnsNotFound()
    {
        // Arrange
        User expectedUser = null;
        _repoStub.Setup(repo => repo.GetUserAsync(1))
            .ReturnsAsync(expectedUser);

        var controller = new UsersController(_configStub.Object, _repoStub.Object);

        // Act
        var actionResult = await controller.GetUserAsync(1);

        // Assert
        actionResult.Result.Should().BeOfType<NotFoundResult>();
    }

    [Fact]
    public async Task GetUsersAsync_WithEmptySearchValue_ReturnsOkEmptyList()
    {
        // Arrange
        string search = "";
        var expectedUsers = new List<User>();
        _repoStub.Setup(repo => repo.GetUsersAsync(search))
            .ReturnsAsync(expectedUsers);

        var controller = new UsersController(_configStub.Object, _repoStub.Object);

        // Act
        var actionResult = await controller.GetUsersAsync(search);

        // Assert
        var okResult = actionResult.Should().BeOfType<OkObjectResult>().Subject;
        var users = okResult.Value.Should().BeAssignableTo<IEnumerable<UserDto>>().Subject;
        int i = 0;
        foreach (UserDto dto in users as IEnumerable<UserDto>)
        {
            // Just counting
            i++;
        }
        i.Should().Be(expectedUsers.Count);
    }

    [Fact]
    public async Task GetUsersAsync_WithSearchValueMatchingResults_ReturnsOkListOfUsers()
    {
        // Arrange
        string search = "Ok list";
        var expectedUsers = new List<User>() { CreateRandomUser(), CreateRandomUser(), CreateRandomUser() };
        _repoStub.Setup(repo => repo.GetUsersAsync(search))
            .ReturnsAsync(expectedUsers);

        var controller = new UsersController(_configStub.Object, _repoStub.Object);

        // Act
        var actionResult = await controller.GetUsersAsync(search);

        // Assert
        var okResult = actionResult.Should().BeOfType<OkObjectResult>().Subject;
        var users = okResult.Value.Should().BeAssignableTo<IEnumerable<UserDto>>().Subject;
        int i = 0;
        foreach (UserDto dto in users as IEnumerable<UserDto>)
        {
            // Just counting
            i++;
        }
        i.Should().Be(expectedUsers.Count);
    }

    [Fact]
    public async Task GetUsersAsync_WithSearchValueMatchingNoResults_ReturnsOkEmptyListOfUsers()
    {
        // Arrange
        string search = "no result expected";
        var expectedUsers = new List<User>();
        _repoStub.Setup(repo => repo.GetUsersAsync(search))
            .ReturnsAsync(expectedUsers);

        var controller = new UsersController(_configStub.Object, _repoStub.Object);

        // Act
        var actionResult = await controller.GetUsersAsync(search);

        // Assert
        var okResult = actionResult.Should().BeOfType<OkObjectResult>().Subject;
        var users = okResult.Value.Should().BeAssignableTo<IEnumerable<UserDto>>().Subject;
        int i = 0;
        foreach (UserDto dto in users as IEnumerable<UserDto>)
        {
            // Just counting
            i++;
        }
        i.Should().Be(expectedUsers.Count);
    }

    [Fact]
    public async Task ValidateUserAsync_WithCorrectInputAndAccountNotValidated_ReturnsUserInfos()
    {
        // Arrange
        User expectedUser = CreateRandomUser();
        _repoStub.Setup(repo => repo.ValidateUserAsync("mail", "validation"))
            .ReturnsAsync(expectedUser);

        var controller = new UsersController(_configStub.Object, _repoStub.Object);

        // Act
        var actionResult = await controller.ValidateUser("mail", "validation");

        // Assert
        var okResult = actionResult.Result as OkObjectResult;
        okResult.Value.Should().BeOfType<UserDto>();
    }



    // [Fact]
    // public async Task ValidateUserAsync_WithCorrectInputAndAccountAlreadyValidated_ReturnsConflictAlreadyActivated()
    // {
    //     // Arrange
    //     User expectedUser = CreateRandomUser();
    //     _repoStub.Setup(repo => repo.ValidateUserAsync("mail", "validation"))
    //         .ReturnsAsync(expectedUser);

    //     var controller = new UsersController(_repoStub.Object);

    //     // Act
    //     var actionResult = await controller.ValidateUser("mail", "validation");

    //     // Assert
    //     var okResult = actionResult.Result as OkObjectResult;
    //     okResult.Value.Should().BeOfType<UserDto>();
    // }

    // [Fact]
    // public async Task ValidateUserAsync_WithCorrectInputAndAccountAlreadyValidated_ReturnsConflictMailOrTokenIncorrect()
    // {
    //     // Arrange
    //     User expectedUser = CreateRandomUser();
    //     _repoStub.Setup(repo => repo.ValidateUserAsync("mail", "validation"))
    //         .ReturnsAsync(expectedUser);

    //     var controller = new UsersController(_repoStub.Object);

    //     // Act
    //     var actionResult = await controller.ValidateUser("mail", "validation");

    //     // Assert
    //     var okResult = actionResult.Result as OkObjectResult;
    //     okResult.Value.Should().BeOfType<UserDto>();
    // }

    [Fact]
    public async Task CreateUserAsync_WithCorrectInput_ReturnsCreatedWithCreatedUser()
    {
        // Arrange
        User expectedUser = CreateRandomUser();
        _repoStub.Setup(repo => repo.CreateUserAsync(expectedUser))
            .ReturnsAsync(1);

        var controller = new UsersController(_configStub.Object, _repoStub.Object);

        // Act
        var actionResult = await controller.CreateUserAsync(new CreateUserDto() { Mail = "sand.dossantos@live.fr" });

        // Assert
        actionResult.Result.Should().BeOfType<CreatedResult>();
    }

    // [Fact]
    // public async Task CreateUserAsync_WithAlreadyUsedMail_ReturnsConflictWithRegisterException()
    // {
    //     // Arrange
    //     User expectedUser = CreateRandomUser();
    //     _repoStub.Setup(repo => repo.CreateUserAsync(expectedUser))
    //         .ThrowsAsync(new RegisterException(RegisterException.GetMessage(-1)));

    //     var controller = new UsersController(_repoStub.Object);

    //     // Act
    //     var actionResult = await controller.CreateUserAsync(new CreateUserDto());

    //     // Assert
    //     actionResult.Result.Should().BeOfType<ConflictResult>(RegisterException.GetMessage(-1));
    // }

    // [Fact]
    // public async Task CreateUserAsync_WithAlreadyUsedUsername_ReturnsConflictWithRegisterException()
    // {
    //     // Arrange
    //     User expectedUser = CreateRandomUser();
    //     _repoStub.Setup(repo => repo.CreateUserAsync(expectedUser))
    //         .ThrowsAsync(new RegisterException(RegisterException.GetMessage(-2)));

    //     var controller = new UsersController(_repoStub.Object);

    //     // Act
    //     var actionResult = await controller.CreateUserAsync(new CreateUserDto());

    //     // Assert
    //     actionResult.Result.Should().BeOfType<ConflictResult>(RegisterException.GetMessage(-2));
    // }
}