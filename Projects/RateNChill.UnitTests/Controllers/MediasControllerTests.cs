using System.Collections.Generic;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using RateNChill.Controllers;
using RateNChill.Dtos.Requests;
using RateNChill.Dtos.Responses;
using RateNChill.Entities;
using RateNChill.Entities.Medias;
using RateNChill.Repositories;
using Xunit;
using static RateNChill.UnitTests.Utils.ObjectGenerators;

namespace RateNChill.UnitTests.Controllers;

public class MediasControllerTests
{
    private readonly Mock<IMediasRepository> _medRepoStub = new Mock<IMediasRepository>();
    private readonly Mock<IMovieDbRepository> _mdbRepoStub = new Mock<IMovieDbRepository>();

    [Fact]
    public async Task GetMediasByTypeAsync_WithExistingType_ReturnsExpectedMedias()
    {
        // Arrange
        var expectedMedias = new List<Media>() { CreateRandomMedia(), CreateRandomMedia(), CreateRandomMedia() };
        _mdbRepoStub.Setup(repo => repo.GetMediasAsync(1, ""))
            .ReturnsAsync(expectedMedias);

        var controller = new MediasController(_medRepoStub.Object, _mdbRepoStub.Object);

        // Act
        var actionResult = await controller.GetMediasByTypeAsync(1);

        // Assert
        var okResult = actionResult.Should().BeOfType<OkObjectResult>().Subject;
        var medias = okResult.Value.Should().BeAssignableTo<IEnumerable<MediaDto>>().Subject;
        int i = 0;
        foreach (MediaDto dto in medias as IEnumerable<MediaDto>)
        {
            dto.ID.Should().Be(expectedMedias[i].id);
            i++;
        }
    }

    [Fact]
    public async Task GetMediasByTypeAsync_WithNonExistingType_ReturnsMovieType()
    {
        // Arrange
        var expectedMedias = new List<Media>() { CreateRandomMedia(), CreateRandomMedia(), CreateRandomMedia() };
        _mdbRepoStub.Setup(repo => repo.GetMediasAsync(1, ""))
            .ReturnsAsync(expectedMedias);

        var controller = new MediasController(_medRepoStub.Object, _mdbRepoStub.Object);

        // Act
        var actionResult = await controller.GetMediasByTypeAsync(25);

        // Assert
        var okResult = actionResult.Should().BeOfType<OkObjectResult>().Subject;
        var medias = okResult.Value.Should().BeAssignableTo<IEnumerable<MediaDto>>().Subject;
        int i = 0;
        foreach (MediaDto dto in medias as IEnumerable<MediaDto>)
        {
            dto.ID.Should().Be(expectedMedias[i].id);
            i++;
        }
    }

    [Fact]
    public async Task GetMediasByNameAsync_WithExistingTypeAndExistingName_ReturnsExpectedMedias()
    {
        // Arrange
        string testName = "TestName";
        var expectedMedias = new List<Media>() { CreateRandomMedia(testName), CreateRandomMedia(testName), CreateRandomMedia(testName) };
        _mdbRepoStub.Setup(repo => repo.GetMediasAsync(1, testName))
            .ReturnsAsync(expectedMedias);

        var controller = new MediasController(_medRepoStub.Object, _mdbRepoStub.Object);

        // Act
        var actionResult = await controller.GetMediasByNameAsync(1, testName);

        // Assert
        var okResult = actionResult.Should().BeOfType<OkObjectResult>().Subject;
        var medias = okResult.Value.Should().BeAssignableTo<IEnumerable<MediaDto>>().Subject;
        int i = 0;
        foreach (MediaDto dto in medias as IEnumerable<MediaDto>)
        {
            dto.ID.Should().Be(expectedMedias[i].id);
            i++;
        }
    }

    [Fact]
    public async Task GetMediasByNameAsync_WithExistingTypeAndNonExistingName_ReturnsEmptyListOfMedias()
    {
        // Arrange
        string testName = "TestName";
        var expectedMedias = new List<Media>();
        _mdbRepoStub.Setup(repo => repo.GetMediasAsync(1, testName))
            .ReturnsAsync(expectedMedias);

        var controller = new MediasController(_medRepoStub.Object, _mdbRepoStub.Object);

        // Act
        var actionResult = await controller.GetMediasByNameAsync(1, testName);

        // Assert
        var okResult = actionResult.Should().BeOfType<OkObjectResult>().Subject;
        var medias = okResult.Value.Should().BeAssignableTo<IEnumerable<MediaDto>>().Subject;
        int i = 0;
        foreach (MediaDto dto in medias as IEnumerable<MediaDto>)
        {
            i++;
        }
        i.Should().Be(0);
    }

}