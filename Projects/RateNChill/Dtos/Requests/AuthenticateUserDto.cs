using System.ComponentModel.DataAnnotations;
using RateNChill.Utils;

namespace RateNChill.Dtos.Requests{
    public record AuthenticateUserDto{
        [Required]
        [EmailAddress]
        public string Mail { get; set; }

        [Required]
        [RegularExpression(RegExpressions.PASSW_REGEX, 
         ErrorMessage = RegExpressions.PASSW_ERROR)]
        public string ShaPassword { get; set; }
    }
}