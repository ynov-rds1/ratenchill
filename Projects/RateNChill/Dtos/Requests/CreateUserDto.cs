using System.ComponentModel.DataAnnotations;
using RateNChill.Utils;

namespace RateNChill.Dtos.Requests {
    public record CreateUserDto{
        [Required]
        [RegularExpression(RegExpressions.NAME_REGEX, 
         ErrorMessage = RegExpressions.NAME_ERROR)]
        public string Name { get; set; }

        [Required]
        [RegularExpression(RegExpressions.NAME_REGEX, 
         ErrorMessage = RegExpressions.NAME_ERROR)]
        public string Firstname { get; set; }

        [Required]
        [RegularExpression(RegExpressions.USNM_REGEX, 
         ErrorMessage = RegExpressions.USNM_ERROR)]
        public string Username { get; set; }

        [Required]
        [EmailAddress]
        public string Mail { get; set; }

        [Required]
        [RegularExpression(RegExpressions.PASSW_REGEX, 
         ErrorMessage = RegExpressions.PASSW_ERROR)]
        public string Pass { get; set; }

        public int InvitedBy { get; set; }
    }
}