using System.ComponentModel.DataAnnotations;

namespace RateNChill.Dtos.Requests {
    public record UpdateMediaDto {
        [Required]
        public int MediaID { get; set; }
        public string? Comment { get; set; }
        public int GradeIndex { get; set; }
    }
}