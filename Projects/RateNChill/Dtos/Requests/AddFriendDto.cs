using System.ComponentModel.DataAnnotations;

namespace RateNChill.Dtos.Requests{
    public record AddFriendDto {
        [Required]
        public int MyFriendId { get; set; }
    }
}