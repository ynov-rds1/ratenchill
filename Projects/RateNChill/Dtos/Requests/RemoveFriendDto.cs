using System.ComponentModel.DataAnnotations;

namespace RateNChill.Dtos.Requests{
    public record RemoveFriendDto {
        [Required]
        public int MyFriendId { get; set; }
    }
}