using RateNChill.Entities;
using RateNChill.Entities.Medias;

namespace RateNChill.Dtos.Responses {
    public record MediaDto {
        public int ID { get; set; }
        public string? OriginalTitle { get; set; }
        public string? Title { get; set; }
        public string? Overview { get; set; }
        public string? Poster { get; set; }

        public float? AverageScore { get; set; }
        public int? TotalSuggestionsByUsers { get; set; }
        public int? TotalSeenByUsers { get; set; }

        public int? Grade { get; set; }
        public string? Comment { get; set; }

        public IEnumerable<UserDto>? FriendsWhoWantToWatchIt { get; set; }
        public IEnumerable<UserDto>? FriendsWhoSawIt { get; set; }
        public IEnumerable<UserDto>? FriendsWhoSuggestedThisMediaToMe { get; set; }
        public IEnumerable<UserDto>? FriendsISuggestedThisMediaTo { get; set; }
        public IEnumerable<CommentDto>? CommentsFromMyFriends { get; set; }
    }
}