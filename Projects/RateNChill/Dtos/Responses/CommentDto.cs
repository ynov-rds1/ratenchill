namespace RateNChill.Dtos.Responses {
    public record CommentDto {
        public string? Comment { get; set; }
        public UserDto? User { get; set; }
    }
}