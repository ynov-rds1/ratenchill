namespace RateNChill.Dtos.Responses {
    public record UserDto {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Firstname { get; set; }
        public string Username { get; set; }
        public string Mail { get; set; }
    }
}