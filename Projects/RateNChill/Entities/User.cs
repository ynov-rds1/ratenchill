using System;

namespace RateNChill.Entities {
    public record User {
        public int id { get; set; }
        public string name { get; set; }
        public string firstname { get; set; }
        public string username { get; set; }
        public string mail { get; set; }
        public string shaPass { get; set; }
        public DateTime createdOn { get; set; }
        public DateTime validatedOn { get; set; }
        public string validation { get; set; }
        public int invitedBy { get; set; }
    }
}