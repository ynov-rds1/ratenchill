namespace RateNChill.Entities.Medias {
    public record PublicInformations {
        public float? AverageScore { get; set; }
        public int? TotalSuggestionsByUsers { get; set; }
        public int? TotalSeenByUsers { get; set; }
    }
}