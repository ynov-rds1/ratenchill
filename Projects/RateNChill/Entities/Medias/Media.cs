namespace RateNChill.Entities.Medias
{
    public record Media
    {
        public int id { get; set; }
        public string? original_title { get; set; }
        public string? title { get; set; }
        public string? overview { get; set; }
        public string? poster_path { get; set; }

        public PublicInformations? PublicInfos { get; set; }
        public UserInformations? UserInfos { get; set; }
    }
}