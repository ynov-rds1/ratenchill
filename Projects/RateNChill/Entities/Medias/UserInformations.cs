namespace RateNChill.Entities.Medias {
    public record UserInformations {
        // Detailed
        public int? Grade { get; set; }
        public string? Comment { get; set; }

        public IEnumerable<User>? FriendsWhoWantToWatchIt { get; set; }
        public IEnumerable<User>? FriendsWhoSawIt { get; set; }
        public IEnumerable<User>? FriendsWhoSuggestedThisMediaToMe { get; set; }
        public IEnumerable<User>? FriendsISuggestedThisMediaTo { get; set; }
        public IEnumerable<Comment>? CommentsFromMyFriends { get; set; }
    }

    public record Comment {
        public string? Comment_Value { get; set; }
        public User? User { get; set; }
    }
}