using RateNChill.Entities.Medias;

namespace RateNChill.Entities {
    public record MediaResult {
        public Media[] results { get; set; }
    }
}