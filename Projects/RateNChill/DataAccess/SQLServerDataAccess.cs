namespace RateNChill.DataAccess {
    public static class SqlServerDataAccess
    {
        public static string GetConnectionString()
        {
#if DEBUG
            return "Server=(localdb)\\MSSQLLocalDB;Database=rateNChill;Trusted_Connection=True;";
#else
            return "Server=(localdb)\\MSSQLLocalDB;Database=rateNChill;Trusted_Connection=True;";
#endif
        }
    }
}