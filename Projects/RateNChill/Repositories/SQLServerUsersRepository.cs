using System;
using System.Data;
using RateNChill.DataAccess;
using RateNChill.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Dapper;
using System.Linq;
using RateNChill.Exceptions;

namespace RateNChill.Repositories
{
    public class SQLServerUsersRepository : IUsersRepository
    {
        #region CONSTANTS

        #endregion

        #region INIT
        private readonly string _database;
        public SQLServerUsersRepository()
        {
            _database = SqlServerDataAccess.GetConnectionString();
        }
        #endregion


        #region IMPLEMENTATIONS
        public async Task<User> GetUserAsync(int id)
        {
            using (IDbConnection cnn = new SqlConnection(this._database))
            {
                var p = new DynamicParameters();
                p.Add("@userID", id);
                string sql = $@"SELECT * FROM DBO.RATER WHERE id = @userID";
                User result = await Task.Run(() => cnn.QueryFirstOrDefaultAsync<User>(sql, p));
                if (result != null) return result;
                else throw new UserNotFoundException();
            }
        }
        public async Task<IEnumerable<User>> GetUsersAsync(string searchValue = "")
        {
            if (!string.IsNullOrWhiteSpace(searchValue))
            {
                using (IDbConnection cnn = new SqlConnection(this._database))
                {
                    var p = new DynamicParameters();
                    p.Add("@searchValue", searchValue);
                    string sql = $@"EXEC DBO.sp_GetUsers @searchValue";
                    return await Task.Run(() => cnn.QueryAsync<User>(sql, p));
                }
            }
            return new List<User>();
        }
        public async Task<User> GetUserByMailAsync(string mail)
        {
            using (IDbConnection cnn = new SqlConnection(this._database))
            {
                var p = new DynamicParameters();
                p.Add("@mail", mail);
                string sql = $@"EXEC DBO.sp_GetUserByMail @mail";
                User result = await Task.Run(() => cnn.QueryFirstOrDefaultAsync<User>(sql, p));
                if (result != null) return result;
                else throw new UserNotFoundException();
            }
        }

        public async Task<bool> CheckPasswordAsync(User user, string pass)
        {
            using (IDbConnection cnn = new SqlConnection(this._database))
            {
                var p = new DynamicParameters();
                p.Add("@id", user.id);
                p.Add("@pass", pass);

                string sql = $@"EXEC DBO.sp_CheckPassword @id, @pass";
                int result = await Task.Run(() => cnn.QueryFirstOrDefaultAsync<int>(sql, p));
                if (result < 1) throw new CheckPasswordException(CheckPasswordException.GetMessage(result));
                else return true;
            }
        }

        public async Task<int> CreateUserAsync(User user)
        {
            using (IDbConnection cnn = new SqlConnection(this._database))
            {
                var p = new DynamicParameters();
                p.Add("@name", user.name);
                p.Add("@firstname", user.firstname);
                p.Add("@username", user.username);
                p.Add("@mail", user.mail);
                p.Add("@shaPassword", user.shaPass);
                p.Add("@validation", user.validation);
                p.Add("@invitedBy", user.invitedBy);

                string sql = $@"EXEC DBO.sp_CreateRater @name, @firstname, @username, @mail, @shaPassword, @validation, @invitedBy";

                cnn.Open();
                using (var trans = cnn.BeginTransaction())
                {
                    int return_value = 0;

                    try
                    {
                        return_value = cnn.Query<int>(sql, p, trans).FirstOrDefault();
                        if (return_value <= 0) throw new RegisterException(RegisterException.GetMessage(return_value));

                        trans.Commit();
                        return return_value;
                    }
                    catch (Exception ex)
                    {
                        if (ex.GetType() == typeof(RegisterException))
                            throw ex;

                        Console.WriteLine($"Error : { ex.Message }");

                        trans.Rollback();
                    }
                }
            }
            await Task.CompletedTask;
            return 0;
        }

        public async Task<User> ValidateUserAsync(string mail, string validation)
        {
            using (IDbConnection cnn = new SqlConnection(this._database))
            {
                var p = new DynamicParameters();
                p.Add("@mail", mail);
                p.Add("@validation", validation);

                string sql = $@"EXEC DBO.sp_ValidateRater @mail, @validation";

                cnn.Open();
                using (var trans = cnn.BeginTransaction())
                {
                    int return_value = 0;

                    try
                    {
                        return_value = cnn.Query<int>(sql, p, trans).FirstOrDefault();
                        if (return_value <= 0) throw new ValidateException(ValidateException.GetMessage(return_value));

                        trans.Commit();
                        return await GetUserAsync(return_value);
                    }
                    catch (Exception ex)
                    {
                        if (ex.GetType() == typeof(ValidateException))
                            throw ex;

                        Console.WriteLine($"Error : { ex.Message }");

                        trans.Rollback();
                    }
                }
            }
            await Task.CompletedTask;
            return new User();
        }

        public async Task<IEnumerable<User>> GetFriendListAsync(string mail, string procedure){
            if (!string.IsNullOrWhiteSpace(mail))
            {
                using (IDbConnection cnn = new SqlConnection(this._database))
                {
                    var p = new DynamicParameters();
                    p.Add("@userMail", mail);
                    string sql = $@"EXEC DBO.{procedure} @userMail";
                    return await Task.Run(() => cnn.QueryAsync<User>(sql, p));
                }
            }
            return new List<User>();
        }

        public async Task<IEnumerable<User>> GetFriendListAsync(string? mail)
        {
            return await GetFriendListAsync(mail, "sp_GetFriendList");
        }

        public async Task<IEnumerable<User>> GetFriendRequestsReceivedAsync(string? mail)
        {
            return await GetFriendListAsync(mail, "sp_GetFriendRequestsReceived");
        }

        public async Task<IEnumerable<User>> GetPendingFriendRequestsSentAsync(string? mail)
        {
            return await GetFriendListAsync(mail, "sp_GetPendingFriendRequestsSent");
        }

        public async Task<int> AddFriendAsync(string userMail, int friendId)
        {
            using (IDbConnection cnn = new SqlConnection(this._database))
            {
                var p = new DynamicParameters();
                p.Add("@userMail", userMail);
                p.Add("@friendId", friendId);

                string sql = $@"EXEC DBO.sp_AddFriend @userMail, @friendId";

                cnn.Open();
                using (var trans = cnn.BeginTransaction())
                {
                    int return_value = 0;

                    try
                    {
                        return_value = await Task.Run(() => cnn.Query<int>(sql, p, trans).FirstOrDefault());
                        if (return_value <= 0) throw new AddFriendException(AddFriendException.GetMessage(return_value));

                        trans.Commit();
                        return return_value;
                    }
                    catch (Exception ex)
                    {
                        if (ex.GetType() == typeof(AddFriendException))
                            throw ex;

                        Console.WriteLine($"Error : { ex.Message }");

                        trans.Rollback();
                    }
                }
            }
            return 0;
        }

        public async Task<int> RemoveFriendAsync(string userMail, int friendId)
        {
            using (IDbConnection cnn = new SqlConnection(this._database))
            {
                var p = new DynamicParameters();
                p.Add("@userMail", userMail);
                p.Add("@friendId", friendId);

                string sql = $@"EXEC DBO.sp_RemoveFriend @userMail, @friendId";

                cnn.Open();
                using (var trans = cnn.BeginTransaction())
                {
                    int return_value = 0;

                    try
                    {
                        return_value = await Task.Run(() => cnn.Query<int>(sql, p, trans).FirstOrDefault());
                        if (return_value <= 0) throw new RemoveFriendException(RemoveFriendException.GetMessage(return_value));

                        trans.Commit();
                        return return_value;
                    }
                    catch (Exception ex)
                    {
                        if (ex.GetType() == typeof(RemoveFriendException))
                            throw ex;

                        Console.WriteLine($"Error : { ex.Message }");

                        trans.Rollback();
                    }
                }
            }
            return 0;
        }
    }
    #endregion
}
