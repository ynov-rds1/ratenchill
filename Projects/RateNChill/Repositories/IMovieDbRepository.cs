using RateNChill.Entities;
using RateNChill.Entities.Medias;

namespace RateNChill.Repositories {
    public interface IMovieDbRepository {
        Task<IEnumerable<Media>> GetMediasAsync(int type, string name = "");
        Task<IEnumerable<Media>> GetMediasByGenreAsync(int type, int genre = 1);
    }
}