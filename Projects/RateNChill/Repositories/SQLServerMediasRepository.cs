using System;
using System.Data;
using RateNChill.DataAccess;
using RateNChill.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Dapper;
using System.Linq;
using RateNChill.Exceptions;
using RateNChill.Entities.Medias;

namespace RateNChill.Repositories
{
    public class SQLServerMediasRepository : IMediasRepository
    {
        #region CONSTANTS

        #endregion

        #region INIT
        private readonly string _database;
        public SQLServerMediasRepository()
        {
            _database = SqlServerDataAccess.GetConnectionString();
        }
        #endregion

        #region IMPLEMENTATIONS
        public async Task SaveMediasAsync(Media[] medias)
        {
            using (IDbConnection cnn = new SqlConnection(this._database))
            {
                var p = new DynamicParameters();

                foreach (Media media in medias)
                {
                    p = new DynamicParameters();
                    p.Add("@id", media.id);
                    p.Add("@original_title", media.original_title);
                    p.Add("@title", media.title);
                    p.Add("@overview", media.overview);
                    p.Add("@poster_path", media.poster_path);

                    string sql = $@"EXEC DBO.sp_SaveMedia @id, @original_title, @title, @overview, @poster_path";
                    await Task.Run(() => cnn.ExecuteAsync(sql, p));
                }
            }
        }

        public async Task<IEnumerable<Media>> GetMediasAsync(int[] ids, string? mail)
        {
            List<Media> medias = new();
            foreach (int id in ids)
            {
                medias.Add(await Task.Run(() => GetMediaAsync(id, mail, false)));
            }
            return medias;
        }

        public async Task<Media> GetMediaAsync(int id, string? mail, bool detailed = true)
        {
            using (IDbConnection cnn = new SqlConnection(this._database))
            {
                var p = new DynamicParameters();
                p.Add("@id", id);

                string sql = $@"EXEC DBO.sp_GetMedia @id";
                Media media = await Task.Run(() => cnn.QueryFirstOrDefaultAsync<Media>(sql, p));

                if (media != null)
                {
                    sql = $@"EXEC DBO.sp_GetMediaPublicInfos @id";
                    media.PublicInfos = await Task.Run(() => cnn.QueryFirstOrDefaultAsync<PublicInformations>(sql, p));

                    if (!string.IsNullOrWhiteSpace(mail) && detailed)
                    {
                        p.Add("@mail", mail);
                        sql = $@"EXEC DBO.sp_GetMediaUserInfos @id, @mail";
                        media.UserInfos = cnn.QueryFirstOrDefault<UserInformations>(sql, p) ?? new UserInformations();

                        sql = $@"EXEC DBO.sp_GetMediaComments @id, @mail";
                        media.
                        UserInfos.
                        CommentsFromMyFriends = await Task.Run(() =>
                                cnn.QueryAsync<Comment, User, Comment>(sql,
                                (comm, user) =>
                                {
                                    comm.User = user;
                                    return comm;
                                }, p,
                                splitOn: "id"));

                        sql = $@"EXEC DBO.sp_GetMediaFriendsWhoWantToWatchIt @id, @mail";
                        media.UserInfos.FriendsWhoWantToWatchIt = await Task.Run(() => cnn.QueryAsync<User>(sql, p));

                        sql = $@"EXEC DBO.sp_GetMediaFriendsWhoSawIt @id, @mail";
                        media.UserInfos.FriendsWhoSawIt = await Task.Run(() => cnn.QueryAsync<User>(sql, p));





                        // sql = $@"EXEC DBO.sp_GetMediaFriendsISuggestedThisMediaTo @id, @mail";
                        // media.UserInfos.FriendsISuggestedThisMediaTo = await Task.Run(() => cnn.QueryAsync<User>(sql, p));

                        // sql = $@"EXEC DBO.sp_GetMediaFriendsWhoSuggestedThisMediaToMe @id, @mail";
                        // media.UserInfos.FriendsWhoSuggestedThisMediaToMe = await Task.Run(() => cnn.QueryAsync<User>(sql, p));
                    }
                }

                return media;
            }
        }

        public async Task<int[]> GetWatchListAsync(string? mail)
        {
            using (IDbConnection cnn = new SqlConnection(this._database))
            {
                var p = new DynamicParameters();
                p.Add("@userMail", mail);

                string sql = $@"EXEC DBO.sp_GetWatchListByMail @userMail";
                var ids = await Task.Run(() => cnn.QueryAsync<int>(sql, p));
                return ids.ToArray();
            }
        }
        public async Task<int[]> GetWatchListAsync(int id)
        {
            using (IDbConnection cnn = new SqlConnection(this._database))
            {
                var p = new DynamicParameters();
                p.Add("@userId", id);

                string sql = $@"EXEC DBO.sp_GetWatchListById @userId";
                var ids = await Task.Run(() => cnn.QueryAsync<int>(sql, p));
                return ids.ToArray();
            }
        }

        public async Task<int> MarkMediaAsToWatchAsync(int mediaID, string? mail)
        {
            using (IDbConnection cnn = new SqlConnection(this._database))
            {
                var p = new DynamicParameters();
                p.Add("@userMail", mail);
                p.Add("@mediaId", mediaID);

                string sql = $@"EXEC DBO.sp_MarkMediaAsToWatch @userMail, @mediaId";

                cnn.Open();
                using (var trans = cnn.BeginTransaction())
                {
                    int return_value = 0;

                    try
                    {
                        return_value = await Task.Run(() => cnn.Query<int>(sql, p, trans).FirstOrDefault());
                        if (return_value <= 0) throw new MediaToWatchException(MediaToWatchException.GetMessage(return_value));

                        trans.Commit();
                        return return_value;
                    }
                    catch (Exception ex)
                    {
                        if (ex.GetType() == typeof(MediaToWatchException))
                            throw ex;

                        Console.WriteLine($"Error : { ex.Message }");

                        trans.Rollback();
                    }
                }
            }
            return 0;
        }

        public async Task<int[]> GetSeenListAsync(string? mail)
        {
            using (IDbConnection cnn = new SqlConnection(this._database))
            {
                var p = new DynamicParameters();
                p.Add("@userMail", mail);

                string sql = $@"EXEC DBO.sp_GetSeenListByMail @userMail";
                var ids = await Task.Run(() => cnn.QueryAsync<int>(sql, p));
                return ids.ToArray();
            }
        }

        public async Task<int[]> GetSeenListAsync(int id)
        {
            using (IDbConnection cnn = new SqlConnection(this._database))
            {
                var p = new DynamicParameters();
                p.Add("@userId", id);

                string sql = $@"EXEC DBO.sp_GetSeenListById @userId";
                var ids = await Task.Run(() => cnn.QueryAsync<int>(sql, p));
                return ids.ToArray();
            }
        }

        public async Task<int> MarkMediaAsSeenAsync(int mediaID, string? mail)
        {
            using (IDbConnection cnn = new SqlConnection(this._database))
            {
                var p = new DynamicParameters();
                p.Add("@userMail", mail);
                p.Add("@mediaId", mediaID);

                string sql = $@"EXEC DBO.sp_MarkMediaAsSeen @userMail, @mediaId";

                cnn.Open();
                using (var trans = cnn.BeginTransaction())
                {
                    int return_value = 0;

                    try
                    {
                        return_value = await Task.Run(() => cnn.Query<int>(sql, p, trans).FirstOrDefault());
                        if (return_value <= 0) throw new MediaSeenException(MediaSeenException.GetMessage(return_value));

                        trans.Commit();
                        return return_value;
                    }
                    catch (Exception ex)
                    {
                        if (ex.GetType() == typeof(MediaSeenException))
                            throw ex;

                        Console.WriteLine($"Error : { ex.Message }");

                        trans.Rollback();
                    }
                }
            }
            return 0;
        }

        public async Task<int> CommentMediaAsync(int mediaID, string? mail, string comment)
        {
            using (IDbConnection cnn = new SqlConnection(this._database))
            {
                var p = new DynamicParameters();
                p.Add("@userMail", mail);
                p.Add("@mediaId", mediaID);
                p.Add("@comment", comment);

                string sql = $@"EXEC DBO.sp_CommentMedia @mediaId, @userMail, @comment";

                cnn.Open();
                using (var trans = cnn.BeginTransaction())
                {
                    int return_value = 0;

                    try
                    {
                        return_value = await Task.Run(() => cnn.Query<int>(sql, p, trans).FirstOrDefault());
                        if (return_value <= 0) throw new CommentMediaException(CommentMediaException.GetMessage(return_value));

                        trans.Commit();
                        return return_value;
                    }
                    catch (Exception ex)
                    {
                        if (ex.GetType() == typeof(CommentMediaException))
                            throw ex;

                        Console.WriteLine($"Error : { ex.Message }");

                        trans.Rollback();
                    }
                }
            }
            return 0;
        }

        public async Task<int> RateMediaAsync(int mediaID, string? mail, int gradeIndex)
        {
            using (IDbConnection cnn = new SqlConnection(this._database))
            {
                var p = new DynamicParameters();
                p.Add("@userMail", mail);
                p.Add("@mediaId", mediaID);
                p.Add("@grade", gradeIndex);

                string sql = $@"EXEC DBO.sp_RateMedia @mediaId, @userMail, @grade";

                cnn.Open();
                using (var trans = cnn.BeginTransaction())
                {
                    int return_value = 0;

                    try
                    {
                        return_value = await Task.Run(() => cnn.Query<int>(sql, p, trans).FirstOrDefault());
                        if (return_value <= 0) throw new RateMediaException(RateMediaException.GetMessage(return_value));

                        trans.Commit();
                        return return_value;
                    }
                    catch (Exception ex)
                    {
                        if (ex.GetType() == typeof(RateMediaException))
                            throw ex;

                        Console.WriteLine($"Error : { ex.Message }");

                        trans.Rollback();
                    }
                }
            }
            return 0;
        }

        public async Task<int> SuggestMediaAsync(int mediaID, string? mail, int targetId)
        {
            using (IDbConnection cnn = new SqlConnection(this._database))
            {
                var p = new DynamicParameters();
                p.Add("@userMail", mail);
                p.Add("@mediaId", mediaID);
                p.Add("@target", targetId);

                string sql = $@"EXEC DBO.sp_SuggestMedia @mediaId, @userMail, @target";

                cnn.Open();
                using (var trans = cnn.BeginTransaction())
                {
                    int return_value = 0;

                    try
                    {
                        return_value = await Task.Run(() => cnn.Query<int>(sql, p, trans).FirstOrDefault());
                        if (return_value <= 0) throw new SuggestMediaException(SuggestMediaException.GetMessage(return_value));

                        trans.Commit();
                        return return_value;
                    }
                    catch (Exception ex)
                    {
                        if (ex.GetType() == typeof(SuggestMediaException))
                            throw ex;

                        Console.WriteLine($"Error : { ex.Message }");

                        trans.Rollback();
                    }
                }
            }
            return 0;
        }
        #endregion

    }
}
