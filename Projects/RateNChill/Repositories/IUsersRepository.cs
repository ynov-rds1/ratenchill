using RateNChill.Entities;

namespace RateNChill.Repositories
{
    public interface IUsersRepository
    {
        Task<User> GetUserAsync(int id);
        Task<IEnumerable<User>> GetUsersAsync(string searchValue = "");
        Task<User> GetUserByMailAsync(string mail);
        Task<bool> CheckPasswordAsync(User user, string pass);
        Task<int> CreateUserAsync(User user);
        Task<User> ValidateUserAsync(string mail, string validation);
        Task<int> AddFriendAsync(string userMail, int friendId);
        Task<int> RemoveFriendAsync(string userMail, int friendId);
        Task<IEnumerable<User>> GetFriendListAsync(string? mail);
        Task<IEnumerable<User>> GetFriendRequestsReceivedAsync(string? mail);
        Task<IEnumerable<User>> GetPendingFriendRequestsSentAsync(string? mail);
    }
}