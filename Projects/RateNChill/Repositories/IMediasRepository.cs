using RateNChill.Entities;
using RateNChill.Entities.Medias;

namespace RateNChill.Repositories
{
    public interface IMediasRepository
    {
        Task SaveMediasAsync(Media[] medias);
        Task<IEnumerable<Media>> GetMediasAsync(int[] ids, string? mail);
        Task<Media> GetMediaAsync(int mediaId, string? mail, bool detailed = true);
        Task<int> MarkMediaAsToWatchAsync(int mediaID, string? mail);
        Task<int> MarkMediaAsSeenAsync(int mediaID, string? mail);
        Task<int[]> GetWatchListAsync(string? mail);
        Task<int[]> GetWatchListAsync(int id);
        Task<int[]> GetSeenListAsync(string? mail);
        Task<int[]> GetSeenListAsync(int id);
        Task<int> CommentMediaAsync(int mediaID, string? mail, string comment);
        Task<int> RateMediaAsync(int mediaID, string? mail, int gradeIndex);
        Task<int> SuggestMediaAsync(int mediaID, string? mail, int targetId);
    }
}
