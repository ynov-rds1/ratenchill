using Newtonsoft.Json;
using RateNChill.Entities;
using RateNChill.Entities.Medias;
using RateNChill.Utils;

namespace RateNChill.Repositories
{
    public class MovieDbRepository : IMovieDbRepository
    {
        #region CONSTANTS
        public static readonly string API_KEY = "api_key=51d12a3618bb0628eee16ca549c139f4";
        private static string URL = "https://api.themoviedb.org/3";
        #endregion

        #region IMPLEMENTATIONS
        public async Task<IEnumerable<Media>> GetMediasAsync(int type, string name = "")
        {
            string extens = FormatUrl(type, name);
            string url = URL + extens;
            return await Task.Run(() => GetFromTheMovieDB(url));
        }

        public async Task<IEnumerable<Media>> GetMediasByGenreAsync(int type, int genre = 12)
        {
            string extens = FormatUrl(type, genre);
            string url = URL + extens;
            return await Task.Run(() => GetFromTheMovieDB(url));
        }

        private async Task<IEnumerable<Media>> GetFromTheMovieDB(string url)
        {
            using (var client = new HttpClient())
            {
                //HTTP GET

                var responseTask = client.GetAsync(url);
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsStringAsync();
                    readTask.Wait();

                    return await Task.FromResult(ConvertReceivedData(readTask.Result));
                }
                else
                {
                    return await Task.FromResult(new List<Media>()); ;
                }
            }
        }
        #endregion

        public static string FormatUrl(int mediaType, string name)
        {
            string type = mediaType == 1 ? "movie?" : "tv?";
            if (string.IsNullOrWhiteSpace(name))
                return "/discover/" + type + API_KEY + "&language=en-US&sort_by=popularity.desc&page=1";
            else
                return "/search/" + type + API_KEY + "&query=" + name.FormatWhiteSpace() + "&language=en-US&sort_by=popularity.desc&page=1";
        }

        public static string FormatUrl(int mediaType, int genre)
        {
            string type = mediaType == 1 ? "movie?" : "tv?";
            return "/discover/" + type + API_KEY + "&language=en-US&sort_by=popularity.desc&page=1&with_genres=" + genre;
        }

        private List<Media> ConvertReceivedData(string jsonObject)
        {
            MediaResult res = JsonConvert.DeserializeObject<MediaResult>(jsonObject);
            return res.results.ToList();
        }
    }
}