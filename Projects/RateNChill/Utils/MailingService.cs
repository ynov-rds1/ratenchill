using System.Net;
using System.Net.Mail;
using RateNChill.Entities;
using RateNChill.Exceptions;

namespace RateNChill.Utils
{
    public static class MailingService
    {
        public static string GenerateRegistrationValidationCode()
        {
            return Helpers.RandomString(20);
        }

        public static bool CheckEmailValid(this string mail)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(mail);
                return addr.Address == mail;
            }
            catch
            {
                throw new InvalidEmailException();
            }
        }

        public static void SendValidationMail(string validationUriToken, string sendTo)
        {
            Mail mail = new();
            mail.To = sendTo;
            mail.From = "rdstestsmtp@gmail.com";
            mail.Subject = "Welcome to Rate'N'Chill";
            mail.Body = $"<h5>Your registration is almost complete ! Please follow this link : <a>{validationUriToken}</a></h5>";

            SendMail(mail, nameof(SendValidationMail));
        }

        public static void SendMail(Mail mail, string funcNameOrigin)
        {
            using (MailMessage mailMessage = new MailMessage())
            {
                mailMessage.From = new MailAddress(mail.From);
                mailMessage.To.Add(mail.To);
                mailMessage.Subject = mail.Subject;
                mailMessage.Body = mail.Body;
                mailMessage.IsBodyHtml = true;

                using (SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587))
                {
                    smtp.Credentials = new NetworkCredential(mail.From, Helpers.PASSWORD);
                    smtp.UseDefaultCredentials = false;
                    smtp.EnableSsl = true;
                    try
                    {
                        smtp.Send(mailMessage);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine($"Exception caught in {funcNameOrigin}() : {ex.Message}");
                    }
                }
            }
        }
    }
}