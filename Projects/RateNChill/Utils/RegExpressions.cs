namespace RateNChill.Utils
{
    public static class RegExpressions
    {
        public const string NAME_REGEX = @"^([a-zA-Z]+( ?|-?|'?)[a-zA-Z]?){2,40}$";
        public const string NAME_ERROR = "2-40 alphabetical characters, [-], ['] and [ ] allowed.";


        public const string USNM_REGEX = @"^([a-zA-Z0-9]+(\\.?-?|_?)[a-zA-Z0-9]?){6,16}$";
        public const string USNM_ERROR = "6-16 alphanumerical characters, [.], [-] and [_] allowed.";

        // TODO FROM HERE
        public const string PASSW_REGEX = @"^(.{0,7}|[^0-9]*|[^A-Z]\\s)$";
        public const string PASSW_ERROR = "Must contain a number, one upper case letter and at least 8 characters long";

    }
}