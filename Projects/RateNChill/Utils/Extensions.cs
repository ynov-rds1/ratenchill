using RateNChill.Dtos.Requests;
using RateNChill.Dtos.Responses;
using RateNChill.Entities;
using RateNChill.Entities.Medias;

namespace RateNChill.Utils {
    public static class Extensions {
        public static int CheckMediaType (this int type){
            return (type > 2 || type < 1) ? 1 : type;
        }
        public static int CheckMediaGenre (this int genre){
            return genre < 1 ? 1 : genre;
        }

        public static string FormatWhiteSpace(this string str){
            return str.Replace(" ","%20");
        }

        public static UserDto AsDto(this User user, int id = 0){
            UserDto dto = new();
            if(user != null){
                dto.Id = user.id;
                dto.Name = user.name;
                dto.Firstname = user.firstname;
                dto.Username = user.username;
                dto.Mail = user.mail;
            }
            return dto;
        }

        public static CommentDto AsDto(this Comment comment){
            CommentDto dto = new();
            if(comment != null){
                dto.Comment = comment.Comment_Value;
                dto.User = comment.User?.AsDto();
            }
            return dto;
        }

        public static Media? GetById (this List<Media> medias, int id){
            return medias.Where(item => item.id == id).Select(med => med).FirstOrDefault();
        }
            
        public static MediaDto AsDto(this Media media){
            MediaDto dto = new();
            if(media != null){
                dto.ID = media.id;
                dto.Title = media.title;
                dto.OriginalTitle = media.original_title;
                dto.Overview = media.overview;
                dto.Poster = media.poster_path;
                
                dto.AverageScore = media.PublicInfos?.AverageScore;
                dto.TotalSuggestionsByUsers = media.PublicInfos?.TotalSuggestionsByUsers;
                dto.TotalSeenByUsers = media.PublicInfos?.TotalSeenByUsers;

                dto.Grade = media.UserInfos?.Grade;
                dto.Comment = media.UserInfos?.Comment;

                dto.FriendsWhoSawIt = (media.UserInfos?.FriendsWhoSawIt)?.Select(item => item.AsDto());
                dto.FriendsWhoWantToWatchIt = (media.UserInfos?.FriendsWhoWantToWatchIt)?.Select(item => item.AsDto());
                dto.FriendsWhoSuggestedThisMediaToMe = (media.UserInfos?.FriendsWhoSuggestedThisMediaToMe)?.Select(item => item.AsDto());
                dto.FriendsISuggestedThisMediaTo = (media.UserInfos?.FriendsISuggestedThisMediaTo)?.Select(item => item.AsDto());
                dto.CommentsFromMyFriends = (media.UserInfos?.CommentsFromMyFriends)?.Select(item => item.AsDto());
            }
            return dto;
        }
    }
}