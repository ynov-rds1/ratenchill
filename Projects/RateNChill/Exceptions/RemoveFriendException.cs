namespace RateNChill.Exceptions
{
    public class RemoveFriendException : Exception
    {
        public RemoveFriendException(string message) : base(message) { }

        public static string GetMessage(int code)
        {
            switch (code)
            {
                case -1:
                    return "Requesting user not found";
                case 1:
                    return "Job's done";
                default:
                    return $"Something went wrong with the code { code } !";
            }
        }
    }
}