namespace RateNChill.Exceptions
{
    public class CheckPasswordException : Exception
    {
        public CheckPasswordException(string message) : base(message) { }

        public static string GetMessage(int code)
        {
            switch (code)
            {
                case -3:
                    return "User not found !";
                case -2:
                    return "User and password don't match !";
                case -1:
                    return "This account hasn't been validated yet, please check your mailbox !";;
                default:
                    return $"Something went wrong with the code { code } !";
            }
        }
    }
}