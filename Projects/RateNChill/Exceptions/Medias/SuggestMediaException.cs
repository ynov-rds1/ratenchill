namespace RateNChill.Exceptions
{
    public class SuggestMediaException : Exception
    {
        public SuggestMediaException(string message) : base(message) { }

        public static string GetMessage(int code)
        {
            switch (code)
            {
                case -3:
                    return "Can't suggest a Media to yourself";
                case -2:
                    return "Target user not found";
                case -1:
                    return "Requesting user not found";
                case 1:
                    return "Media added to target suggestions";
                case 2:
                    return "Media removed from target suggestions";
                default:
                    return $"Something went wrong with the code { code } !";
            }
        }
    }
}
