namespace RateNChill.Exceptions
{
    public class MediaSeenException : Exception
    {
        public MediaSeenException(string message) : base(message) { }

        public static string GetMessage(int code)
        {
            switch (code)
            {
                case -1:
                    return "Requesting user not found";
                case 1:
                    return "Media added to seen list";
                case 2:
                    return "Media removed from seen list";
                default:
                    return $"Something went wrong with the code { code } !";
            }
        }
    }
}