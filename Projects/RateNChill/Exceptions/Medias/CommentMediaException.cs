namespace RateNChill.Exceptions
{
    public class CommentMediaException : Exception
    {
        public CommentMediaException(string message) : base(message) { }

        public static string GetMessage(int code)
        {
            switch (code)
            {
                case -1:
                    return "Requesting user not found";
                case 1:
                    return "Updated comment !";
                case 2:
                    return "Comment saved !";
                default:
                    return $"Something went wrong with the code { code } !";
            }
        }
    }
}