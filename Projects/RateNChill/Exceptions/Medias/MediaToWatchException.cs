namespace RateNChill.Exceptions
{
    public class MediaToWatchException : Exception
    {
        public MediaToWatchException(string message) : base(message) { }

        public static string GetMessage(int code)
        {
            switch (code)
            {
                case -1:
                    return "Requesting user not found";
                case 1:
                    return "Media added to WatchList";
                case 2:
                    return "Media removed from WatchList";
                default:
                    return $"Something went wrong with the code { code } !";
            }
        }
    }
}