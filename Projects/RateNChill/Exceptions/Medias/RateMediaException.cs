namespace RateNChill.Exceptions
{
    public class RateMediaException : Exception
    {
        public RateMediaException(string message) : base(message) { }

        public static string GetMessage(int code)
        {
            switch (code)
            {
                case -1:
                    return "Requesting user not found";
                case 1:
                    return "Updated grade !";
                case 2:
                    return "Grade saved !";
                default:
                    return $"Something went wrong with the code { code } !";
            }
        }
    }
}