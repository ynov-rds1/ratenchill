namespace RateNChill.Exceptions
{
    public class ValidateException : Exception
    {
        public ValidateException(string message) : base(message) { }

        public static string GetMessage(int code)
        {
            switch (code)
            {
                case -2:
                    return "This validation link doesn't work, validation code or mail address isn't valid";
                case -1:
                    return "Account already validated !";
                default:
                    return $"Something went wrong with the code { code } !";
            }
        }
    }
}