namespace RateNChill.Exceptions
{
    public class AddFriendException : Exception
    {
        public AddFriendException(string message) : base(message) { }

        public static string GetMessage(int code)
        {
            switch (code)
            {
                case -5:
                    return "Friend request already sent";
                case -4:
                    return "Already friends !";
                case -3:
                    return "Can't befriend yourself !";
                case -2:
                    return "Target user not found";
                case -1:
                    return "Requesting user not found";
                case 1:
                    return "Friend request accepted";
                case 2:
                    return "Friend request sent";
                default:
                    return $"Something went wrong with the code { code } !";
            }
        }
    }
}