namespace RateNChill.Exceptions
{
    public class InvalidEmailException : Exception
    {
        public InvalidEmailException(string message = "Mail format incorrect !") : base(message) { }
    }
}