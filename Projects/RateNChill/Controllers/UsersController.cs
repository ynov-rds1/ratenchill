using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RateNChill.Dtos.Requests;
using RateNChill.Dtos.Responses;
using RateNChill.Entities;
using RateNChill.Exceptions;
using RateNChill.Repositories;
using RateNChill.Utils;
using static RateNChill.Utils.MailingService;

namespace RateNChill.Controllers
{
    [ApiController]
    [Route("users")]
    public class UsersController : ControllerBase
    {
        private readonly IConfiguration _config;
        private readonly IUsersRepository _userRepo;

        public UsersController(IConfiguration config, IUsersRepository userRepo)
        {
            _config = config;
            _userRepo = userRepo;
        }

        // GET /user/id={id}
        [HttpGet("id={id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<ActionResult<UserDto>> GetUserAsync(int id)
        {
            User user = await _userRepo.GetUserAsync(id);

            if (user is null)
                return NotFound();

            return Ok(user.AsDto());
        }

        // GET /users/{searchValue}
        [HttpGet("{searchValue}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> GetUsersAsync(string searchValue)
        {
            var users = (await _userRepo.GetUsersAsync(searchValue))
                                    .Select(item => item.AsDto());
            return Ok(users);
        }

        // GET /users/mail={mail}/validation={validationToken}
        [HttpGet("mail={mail}&validation={validationToken}")]
        public async Task<ActionResult<UserDto>> ValidateUser(string mail, string validationToken)
        {
            try
            {
                User user = await _userRepo.ValidateUserAsync(mail, validationToken);
                if (user is null)
                    return NotFound();
                return Ok(user.AsDto());
            }
            catch (ValidateException ve)
            {
                return Conflict(new { message = ve.Message });
            }
        }

        // PUT /users
        [HttpPut]
        public async Task<ActionResult<UserDto>> UpdateUserAsync(UpdateUserDto updateUserDto)
        {
            if (ModelState.IsValid)
            {
                // TODO Update User Infos
                return Ok(updateUserDto);
            }
            InvalidEmailException ex = new InvalidEmailException();
            return Conflict(new { message = ex.Message });
        }


        // POST /users
        [HttpPost]
        public async Task<ActionResult<UserDto>> CreateUserAsync(CreateUserDto createUserDto)
        {
            if (ModelState.IsValid)
            {
                User user = new()
                {
                    name = createUserDto.Name,
                    firstname = createUserDto.Firstname,
                    username = createUserDto.Username,
                    mail = createUserDto.Mail,
                    shaPass = createUserDto.Pass,
                    invitedBy = createUserDto.InvitedBy,
                    validation = GenerateRegistrationValidationCode()
                };

                try
                {
                    int userId = await Task.Run(() => _userRepo.CreateUserAsync(user));
                    UserDto dto = user.AsDto(userId);

#if DEBUG
                    // Send mail with validation token
                    string uri = $"https://localhost:7020/users/mail={user.mail}&validation={user.validation}";
                    SendValidationMail(uri, user.mail);
#endif

                    return Created($"[Insert_base_URI_here]/users/{userId}", dto);
                    // return CreatedAtAction("users", new { id = userId }, user.AsDto());
                }
                catch (RegisterException re)
                {
                    return Conflict(new { message = re.Message });
                }
            }
            InvalidEmailException ex = new InvalidEmailException();
            return Conflict(new { message = ex.Message });
        }

        // GET /friendlist
        [HttpGet("friendList")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> GetFriendListAsync()
        {
            string mail = User.FindFirst(ClaimTypes.Name)?.Value;
            var users = (await _userRepo.GetFriendListAsync(mail))
                                    .Select(item => item.AsDto());
            return Ok(users);
        }

        // GET /friendRequestsReceived
        [HttpGet("friendRequestsReceived")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> GetFriendRequestsReceivedAsync()
        {
            string mail = User.FindFirst(ClaimTypes.Name)?.Value;
            var users = (await _userRepo.GetFriendRequestsReceivedAsync(mail))
                                    .Select(item => item.AsDto());
            return Ok(users);
        }

        // GET /pendingFriendRequestsSent
        [HttpGet("pendingFriendRequestsSent")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> GetPendingFriendRequestsSentAsync()
        {
            string mail = User.FindFirst(ClaimTypes.Name)?.Value;
            var users = (await _userRepo.GetPendingFriendRequestsSentAsync(mail))
                                    .Select(item => item.AsDto());
            return Ok(users);
        }

        // POST /addfriend
        [HttpPost("addfriend")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<ActionResult<UserDto>> AddFriendAsync(AddFriendDto addFriendDto)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    string? mail = User.FindFirst(ClaimTypes.Name)?.Value;

                    int code = await _userRepo.AddFriendAsync(mail, addFriendDto.MyFriendId);
                    return Ok(new { message = AddFriendException.GetMessage(code) });
                }
                catch (System.Exception ex)
                {
                    return Conflict(new { message = ex.Message });
                }
            }
            return BadRequest();
        }

        // DELETE /removeFriend
        [HttpDelete("removeFriend")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<ActionResult<UserDto>> RemoveFriendAsync(RemoveFriendDto removeFriendDto)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    string? mail = User.FindFirst(ClaimTypes.Name)?.Value;

                    int code = await _userRepo.RemoveFriendAsync(mail, removeFriendDto.MyFriendId);
                    return Ok(new { message = RemoveFriendException.GetMessage(code) });
                }
                catch (System.Exception ex)
                {
                    return Conflict(new { message = ex.Message });
                }
            }
            return BadRequest();
        }
    }
}
