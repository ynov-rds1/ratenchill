using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RateNChill.Dtos.Responses;
using RateNChill.Dtos.Requests;
using RateNChill.Entities.Medias;
using RateNChill.Repositories;
using RateNChill.Utils;
using RateNChill.Exceptions;

namespace RateNChill.Controllers
{
    [ApiController]
    [Route("medias")]
    public class MediasController : ControllerBase
    {
        private readonly IMediasRepository _mediaRepo;
        private readonly IMovieDbRepository _mdbRepo;

        public MediasController(IMediasRepository medRepo, IMovieDbRepository mdbRepo)
        {
            _mediaRepo = medRepo;
            _mdbRepo = mdbRepo;
        }

        // GET /medias/{type}
        [HttpGet("{type}")]
        public async Task<ActionResult<IEnumerable<MediaDto>>> GetMediasByTypeAsync(int type)
        {
            // Get the Medias from TMDB (1 = movies, 2 = TV Shows)
            type = type.CheckMediaType();
            Media[] medias = (await _mdbRepo.GetMediasAsync(type)).ToArray();

            // Save Medias in SQLServer Database (only unexisting ones)
            await SaveMedias(medias);

            // Get the Medias collected previously with custom informations from RNC Database
            string? mail = User.FindFirst(ClaimTypes.Name)?.Value;
            var result = await GetMedias(medias, mail);

            return Ok(result);
        }

        // GET /medias/{type}/{name}
        [HttpGet("{type}/{name}")]
        public async Task<ActionResult<IEnumerable<MediaDto>>> GetMediasByNameAsync(int type, string name)
        {
            // Get the Medias from TMDB, based on the given name of the Media (1 = movies, 2 = TV Shows)
            type = type.CheckMediaType();
            Media[] medias = (await _mdbRepo.GetMediasAsync(type, name)).ToArray();

            // Save Medias in SQLServer Database (only unexisting ones)
            await SaveMedias(medias);

            // Get the Medias collected previously with custom informations from RNC Database
            string? mail = User.FindFirst(ClaimTypes.Name)?.Value;
            var result = await GetMedias(medias, mail);

            return Ok(result);
        }

        // GET /medias/id={id}
        [HttpGet("id={id}")]
        public async Task<ActionResult<MediaDto>> GetMediaById(int id)
        {
            // Get a specific Media and its informations from RNC Database
            string? mail = User.FindFirst(ClaimTypes.Name)?.Value;
            var result = await GetMedia(id, mail);

            if(result is null) return NotFound(new { message = "Media not found."});

            return Ok(result);
        }

        // GET /medias/towatch
        [HttpGet("towatch")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> GetWatchListAsync()
        {
            // Get the Authenticated User's WatchList
            string? mail = User.FindFirst(ClaimTypes.Name)?.Value;
            int[] ids = await _mediaRepo.GetWatchListAsync(mail);
            var result = await GetMedias(ids, mail);

            return Ok(result);
        }

        // GET /medias/towatch&id={id}
        [HttpGet("towatch&id={id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> GetFriendWatchListAsync(int id)
        {
            // Get the Authenticated User's friend's WatchList (by id)
            string? mail = User.FindFirst(ClaimTypes.Name)?.Value;
            int[] ids = await _mediaRepo.GetWatchListAsync(id);
            var result = await GetMedias(ids, mail);

            return Ok(result);
        }

        // PUT /medias/towatch
        [HttpPut("towatch")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> MarkMediaAsToWatchAsync(UpdateMediaDto mediaDto)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    string? mail = User.FindFirst(ClaimTypes.Name)?.Value;

                    // Toggle Authenticated User's Watch/Seen option in RNC Database for the given Media
                    int code = await _mediaRepo.MarkMediaAsToWatchAsync(mediaDto.MediaID, mail);
                    return Ok(new { message = MediaToWatchException.GetMessage(code) });
                }
                catch (System.Exception ex)
                {
                    return Conflict(new { message = ex.Message });
                }
            }
            return BadRequest();
        }

        // GET /medias/seen
        [HttpGet("seen")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> GetSeenListAsync()
        {
            // Get the Authenticated User's SeenList (by id)
            string? mail = User.FindFirst(ClaimTypes.Name)?.Value;
            int[] ids = await _mediaRepo.GetSeenListAsync(mail);
            var result = await GetMedias(ids, mail);

            return Ok(result);
        }

        // GET /medias/seen&id={id}
        [HttpGet("seen&id={id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> GetFriendSeenListAsync(int id)
        {
            // Get the Authenticated User's friend's SeenList (by id)
            string? mail = User.FindFirst(ClaimTypes.Name)?.Value;
            int[] ids = await _mediaRepo.GetSeenListAsync(id);
            var result = await GetMedias(ids, mail);

            return Ok(result);
        }

        // PUT /medias/seen
        [HttpPut("seen")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> MarkMediaAsSeenAsync(UpdateMediaDto mediaDto)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    string? mail = User.FindFirst(ClaimTypes.Name)?.Value;

                    // Toggle Authenticated User's Watch/Seen option in RNC Database for the given Media
                    int code = await _mediaRepo.MarkMediaAsSeenAsync(mediaDto.MediaID, mail);
                    return Ok(new { message = MediaSeenException.GetMessage(code) });
                }
                catch (System.Exception ex)
                {
                    return Conflict(new { message = ex.Message });
                }
            }
            return BadRequest();
        }

        // POST /medias/comment
        [HttpPost("comment")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> CommentMediaAsync(UpdateMediaDto mediaDto)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    string? mail = User.FindFirst(ClaimTypes.Name)?.Value;

                    // Save Authenticated User's comment on the given Media in RNC Database
                    int code = await _mediaRepo.CommentMediaAsync(mediaDto.MediaID, mail, mediaDto.Comment ?? "");
                    return Ok(new { message = CommentMediaException.GetMessage(code) });
                }
                catch (System.Exception ex)
                {
                    return Conflict(new { message = ex.Message });
                }
            }
            return BadRequest();
        }

        // POST /medias/rate
        [HttpPost("rate")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> RateMediaAsync(UpdateMediaDto mediaDto)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    string? mail = User.FindFirst(ClaimTypes.Name)?.Value;

                    // Save Authenticated User's grade on the given Media in RNC Database
                    int code = await _mediaRepo.RateMediaAsync(mediaDto.MediaID, mail, mediaDto.GradeIndex);
                    return Ok(new { message = RateMediaException.GetMessage(code) });
                }
                catch (System.Exception ex)
                {
                    return Conflict(new { message = ex.Message });
                }
            }
            return BadRequest();
        }

        #region PRIVATE
        private async Task<MediaDto> GetMedia(int mediaId, string? mail)
        {
            return (await _mediaRepo.GetMediaAsync(mediaId, mail)).AsDto();
        }

        private async Task<IEnumerable<MediaDto>> GetMedias(Media[] medias, string? mail)
        {
            int[] ids = new int[medias.Length];
            for (int i = 0; i < medias.Length; i++)
            {
                ids[i] = medias[i].id;
            }

            return (await _mediaRepo.GetMediasAsync(ids, mail))
                        .Select(item => item.AsDto());
        }

        private async Task<IEnumerable<MediaDto>> GetMedias(int[] ids, string? mail)
        {
            return (await _mediaRepo.GetMediasAsync(ids, mail))
                        .Select(item => item.AsDto());
        }

        private async Task SaveMedias(Media[] medias)
        {
            await _mediaRepo.SaveMediasAsync(medias);
        }
        #endregion
    }
}
