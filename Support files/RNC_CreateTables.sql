﻿drop table dbo.RaterMediaList;
drop table dbo.Grade;

drop table dbo.MediaSuggestion;
drop table dbo.Media;
drop table dbo.FriendList;
drop table dbo.FriendRequest;
drop table dbo.Rater;

CREATE TABLE dbo.Rater(
	id				INT NOT NULL IDENTITY,
	[name]			NVARCHAR(255) NOT NULL,
	firstname		NVARCHAR(255) NOT NULL,
	username		NVARCHAR(50) NOT NULL,
	mail			NVARCHAR(255) NOT NULL,
	shaPassword		NVARCHAR(255) NOT NULL,
	createdOn		DATETIME NOT NULL,
	validatedOn		DATETIME NULL,
	[validation]	NVARCHAR(255) NOT NULL,
	token			NVARCHAR(255) NOT NULL,
	invitedBy		INT NULL,

	CONSTRAINT PK_Rater PRIMARY KEY (id),
	CONSTRAINT FK_Rater_InvitedBy FOREIGN KEY (invitedBy) REFERENCES dbo.Rater (id)
);

CREATE TABLE DBO.FriendList(
	user1			INT NOT NULL,
	user2			INT NOT NULL,
	setOn			DATETIME NULL,

	CONSTRAINT PK_FriendList PRIMARY KEY (user1, user2),
	CONSTRAINT FK_FriendList_User1 FOREIGN KEY (user1) REFERENCES DBO.Rater(id),
	CONSTRAINT FK_FriendList_User2 FOREIGN KEY (user2) REFERENCES DBO.Rater(id)
);

CREATE TABLE DBO.FriendRequest(
	sentTo			INT NOT NULL,
	sentBy			INT NOT NULL,
	validated		BIT NOT NULL,

	CONSTRAINT PK_FriendRequest PRIMARY KEY (sentTo, sentBy),
	CONSTRAINT FK_FriendRequest_SentTo FOREIGN KEY (sentTo) REFERENCES DBO.Rater(id),
	CONSTRAINT FK_FriendRequest_SentBy FOREIGN KEY (sentBy) REFERENCES DBO.Rater(id)
);

CREATE TABLE dbo.Media(
	id				INT NOT NULL,
	original_title	NVARCHAR(255) NULL,
	title			NVARCHAR(255) NULL,
	overview		NVARCHAR(255) NULL,
	poster_path		NVARCHAR(255) NULL,

	CONSTRAINT PK_Media PRIMARY KEY (id)
);

CREATE TABLE dbo.MediaSuggestion (
	sender_id			INT NOT NULL,
	receiver_id			INT NOT NULL,
	media_id			INT NOT NULL,
	CONSTRAINT PK_MediaSuggestion PRIMARY KEY (sender_id, receiver_id, media_id),
	CONSTRAINT FK_MediaSuggestion_Sender FOREIGN KEY (sender_id) REFERENCES dbo.Rater (id),
	CONSTRAINT FK_MediaSuggestion_Receiver FOREIGN KEY (receiver_id) REFERENCES dbo.Rater (id),
	CONSTRAINT FK_MediaSuggestion_Media FOREIGN KEY (media_id) REFERENCES dbo.Media (id)
);

CREATE TABLE dbo.Grade (
	id				INT NOT NULL IDENTITY,
	[value]			INT NOT NULL,
	CONSTRAINT PK_Grade PRIMARY KEY (id),
);

INSERT INTO dbo.Grade VALUES (0),(1),(2),(3),(4),(5);

CREATE TABLE dbo.RaterMediaList (
	rater_id			INT NOT NULL,
	media_id			INT NOT NULL,
	toWatch				BIT NOT NULL DEFAULT 0,
	seen				BIT NOT NULL DEFAULT 0,
	comment				TEXT NULL,
	grade				INT NULL,

	CONSTRAINT PK_RaterMediaList  PRIMARY KEY (rater_id, media_id),
	CONSTRAINT FK_RaterMediaList_RaterId FOREIGN KEY (rater_id) REFERENCES dbo.Rater (id),
	CONSTRAINT FK_RaterMediaList_MediaId FOREIGN KEY (media_id) REFERENCES dbo.Media (id),
	CONSTRAINT FK_RaterMediaList_Grade FOREIGN KEY (grade) REFERENCES dbo.Grade (id)
);

CREATE TYPE DBO.TMDB_MEDIA AS TABLE (
	id				INT NOT NULL,
	original_title	NVARCHAR(255) NOT NULL,
	title			NVARCHAR(255) NOT NULL,
	overview		NVARCHAR(255) NOT NULL,
	poster_path		NVARCHAR(255) NOT NULL
);

CREATE TYPE DBO.MEDIAIDS AS TABLE (
	id				INT NOT NULL
);