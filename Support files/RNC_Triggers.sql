﻿CREATE OR ALTER TRIGGER DBO.trg_AddFriendToFriendList
ON DBO.FriendRequest
AFTER INSERT, UPDATE
AS
	DECLARE @user1 INT;
	DECLARE @user2 INT;
	DECLARE @validated BIT;

	SET @user1 = (SELECT i.sentBy FROM inserted i); 
	SET @user2 = (SELECT i.sentTo FROM inserted i); 
	SET @validated = (SELECT i.validated FROM inserted i); 

	IF NOT EXISTS (SELECT * FROM DBO.FriendList f WHERE
				(user1=@user1 AND user2=@user2) OR
				(user1=@user2 AND user2=@user1))
	BEGIN
		IF(@validated = 1)
		BEGIN
			INSERT INTO DBO.FriendList VALUES
			(@user1, @user2, GETDATE());
			DELETE FROM DBO.FriendRequest WHERE 
			(sentTo=@user1 AND sentBy=@user2) OR
			(sentTo=@user2 AND sentBy=@user1)
		END
	END
GO