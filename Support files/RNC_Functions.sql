﻿ALTER FUNCTION DBO.fct_CheckUsernameAlreadyExists 
	(@username NVARCHAR(50))
RETURNS BIT
AS
BEGIN
	DECLARE @usernameExists BIT = 0;
	IF EXISTS (SELECT r.id FROM DBO.Rater r WHERE LOWER(r.username) = LOWER(@username))
	BEGIN
		SET @usernameExists = 1;
	END
	RETURN @usernameExists;
END
GO

ALTER FUNCTION DBO.fct_CheckMailAlreadyExists 
	(@mail NVARCHAR(50))
RETURNS BIT
AS
BEGIN
	DECLARE @mailExists BIT = 0;
	IF EXISTS (SELECT r.id FROM DBO.Rater r WHERE LOWER(r.mail) = LOWER(@mail))
	BEGIN
		SET @mailExists = 1;
	END
	RETURN @mailExists;
END
GO

ALTER FUNCTION DBO.fct_CheckValidationExists 
	(@validation NVARCHAR(255))
RETURNS BIT
AS
BEGIN
	DECLARE @validationExists BIT = 0;
	IF EXISTS (SELECT r.id FROM DBO.Rater r WHERE r.[validation] = @validation)
	BEGIN
		SET @validationExists = 1;
	END
	RETURN @validationExists;
END
GO

ALTER FUNCTION DBO.fct_GetUserIdByMail
	(@mail NVARCHAR(255))
RETURNS INT
AS
BEGIN
DECLARE @userID INT = 0;
IF EXISTS (SELECT TOP 1 r.id FROM DBO.Rater r WHERE LOWER(r.mail) = LOWER(@mail))
BEGIN
	SET @userID = (SELECT TOP 1 r.id FROM DBO.Rater r WHERE LOWER(r.mail) = LOWER(@mail));
END
RETURN @userID;
END
GO

ALTER FUNCTION DBO.fct_GetTotalSuggestions
	(@id INT)
RETURNS INT
AS
BEGIN
RETURN(SELECT SUM(ms.media_id) FROM DBO.MediaSuggestion ms WHERE ms.media_id = @id);
END
GO

ALTER FUNCTION DBO.fct_GetTotalSeen
	(@id INT)
RETURNS INT
AS
BEGIN
DECLARE @totalSeen INT = 0;
SET @totalSeen = (SELECT COUNT(rml.rater_id)
				  FROM DBO.Media m
				  INNER JOIN DBO.RaterMediaList rml ON m.id = rml.media_id
				  WHERE rml.seen = 1 AND m.id = @id);

RETURN @totalSeen;
END
GO

ALTER FUNCTION DBO.fct_CalculateAverageGrade
	(@id INT)
RETURNS DECIMAL(3,2)
AS
BEGIN
DECLARE @avg DECIMAL(3,2) = 0;
SET @avg = (SELECT AVG(ALL g.[value])
			FROM DBO.RaterMediaList rml
			INNER JOIN DBO.Grade g ON g.id = rml.grade
			WHERE rml.grade IS NOT NULL AND rml.media_id = @id);
RETURN @avg;
END
GO

ALTER FUNCTION DBO.fct_CheckIfMediaExists
	(@id INT)
RETURNS BIT
AS
BEGIN
DECLARE @mediaExists BIT = 0;
IF EXISTS (SELECT m.id FROM DBO.Media m WHERE m.id = @id)
BEGIN
	SET @mediaExists = 1;
END
RETURN @mediaExists;
END
GO

ALTER FUNCTION DBO.fct_FriendList
	(@id INT)
RETURNS @ids TABLE (id INT)
AS
BEGIN
INSERT INTO @ids
	SELECT f.user2 FROM DBO.Rater r
	RIGHT OUTER JOIN DBO.FriendList f
	ON r.id = f.user1
	WHERE r.id = @id
	UNION
	SELECT f.user1 FROM DBO.Rater r
	RIGHT OUTER JOIN DBO.FriendList f
	ON r.id = f.user2
	WHERE r.id = @id;
RETURN;
END
GO
