ALTER PROCEDURE DBO.sp_CreateRater
	@name		NVARCHAR(255),
	@firstname	NVARCHAR(255),
	@username	NVARCHAR(50),
	@mail		NVARCHAR(255),
	@shaPass	NVARCHAR(255),
	@validation	NVARCHAR(255),
	@invitedBy	NVARCHAR(255)
AS
DECLARE @return_value INT = 0;

IF DBO.fct_CheckMailAlreadyExists(@mail) = 1
BEGIN
	SET @return_value = -1; -- Mail already exists
END
ELSE IF DBO.fct_CheckUsernameAlreadyExists(@username) = 1
BEGIN
	SET @return_value = -2; -- Username taken
END
ELSE
BEGIN
	IF (@invitedBy > 0)
	BEGIN
	INSERT INTO DBO.Rater VALUES
	(@name, @firstname, @username, @mail, @shaPass, GETDATE(), null, @validation, N'TODO Random', @invitedBy);
	END
	ELSE
	BEGIN
	INSERT INTO DBO.Rater VALUES
	(@name, @firstname, @username, @mail, @shaPass, GETDATE(), null, @validation, N'TODO Random', null);
	END
	SET @return_value = SCOPE_IDENTITY();
END

SELECT @return_value;
GO

ALTER PROCEDURE DBO.sp_ValidateRater
	@mail		NVARCHAR(255),
	@validation	NVARCHAR(255)
AS
DECLARE @return_value INT = 0;
DECLARE @userID INT = (SELECT r.id FROM DBO.Rater r WHERE (LOWER(r.mail)=LOWER(@mail) AND r.[validation] = @validation));

IF (DBO.fct_CheckMailAlreadyExists(@mail) = 0 OR DBO.fct_CheckValidationExists(@validation) = 0)
BEGIN
	SET @return_value = -2;
END
ELSE IF (@userID > 0)
BEGIN
	DECLARE @validatedOn DATETIME = (SELECT r.validatedOn FROM DBO.Rater r WHERE r.id = @userID);
	IF (@validatedOn is null)
	BEGIN
		UPDATE DBO.Rater SET validatedOn=GETDATE() WHERE [validation] = @validation AND LOWER(mail) = LOWER(@mail);
		SET @return_value = (SELECT TOP 1 r.id FROM DBO.Rater r WHERE r.[validation] = @validation AND LOWER(r.mail) = LOWER(@mail));
	END
	ELSE
	BEGIN
		SET @return_value = -1;
	END
END
SELECT @return_value;
GO

ALTER PROCEDURE DBO.sp_GetUserByMail
	@mail NVARCHAR(255)
AS
SELECT * FROM DBO.Rater r WHERE LOWER(r.mail) = LOWER(@mail);
GO

ALTER PROCEDURE DBO.sp_CheckPassword
	@id INT,
	@shaPassword NVARCHAR(255)
AS
DECLARE @return_value INT = 0;
DECLARE @validatedOn DATETIME = null;

IF EXISTS (SELECT r.id FROM DBO.Rater r WHERE r.id = @id)
BEGIN
	SET @validatedOn = (SELECT TOP 1 r.validatedOn FROM DBO.Rater r WHERE r.id = @id);

	IF (@validatedOn is null)
	BEGIN
		SET @return_value = -1; -- Account not valid
	END
	ELSE IF EXISTS (SELECT r.id FROM DBO.Rater r WHERE r.id = @id AND r.shaPassword = @shaPassword)
	BEGIN
		SET @return_value = 1; -- User and Pass match
	END
	ELSE
	BEGIN
		SET @return_value = -2; -- User and Pass don't match
	END
END
ELSE
BEGIN
	SET @return_value = -3 -- User not found
END
SELECT @return_value;
GO

ALTER PROCEDURE DBO.sp_GetUsers
	@searchValue NVARCHAR(255)
AS
SELECT * 
FROM DBO.Rater r
WHERE
CHARINDEX(LOWER(@searchValue), LOWER(r.[name])) > 0 OR
CHARINDEX(LOWER(@searchValue), LOWER(r.[firstname])) > 0 OR
CHARINDEX(LOWER(@searchValue), LOWER(r.[username])) > 0 OR
CHARINDEX(LOWER(@searchValue), LOWER(r.[mail])) > 0
GO

ALTER PROCEDURE DBO.sp_GetFriendList
	@userMail NVARCHAR(255)
AS
DECLARE @userID INT = DBO.fct_GetUserIdByMail(@userMail);
IF (@userID <> 0)
BEGIN
	--DECLARE @friendList TABLE (id INT)
	--DECLARE @relationships TABLE (id INT IDENTITY, id1 INT, id2 INT);
	--INSERT INTO @relationships (id1, id2) 
	--SELECT 
	--	r1.id as id1,
	--	r2.id as id2		
	--FROM DBO.FriendList f
	--INNER JOIN DBO.Rater r1 ON r1.id = f.user1
	--INNER JOIN DBO.Rater r2 ON r2.id = f.user2
	--WHERE @userID = f.user1 OR @userID = f.user2

	--DECLARE @CursorTestID INT = 1;
	--DECLARE @RunningTotal BIGINT = 0;
	--DECLARE @RowCnt BIGINT = 0;

	---- get a count of total rows to process 
	--SELECT @RowCnt = COUNT(0) FROM @relationships;
 
	--WHILE @CursorTestID <= @RowCnt
	--BEGIN
	--	IF EXISTS (SELECT * FROM @relationships r WHERE
	--					r.id = @CursorTestID AND
	--					r.id1 = @userID)
	--	BEGIN
	--		INSERT INTO @friendList 
	--						SELECT r.id2 FROM @relationships r WHERE
	--						r.id = @CursorTestID AND
	--						r.id1 = @userID
	--	END
	--	ELSE IF EXISTS (SELECT * FROM @relationships r WHERE
	--					r.id = @CursorTestID AND
	--					r.id2 = @userID)
	--	BEGIN
	--		INSERT INTO @friendList 
	--						SELECT r.id1 FROM @relationships r WHERE
	--						r.id = @CursorTestID AND
	--						r.id2 = @userID
	--	END
    
	--    SET @CursorTestID = @CursorTestID + 1 
	--END
	DECLARE @friendList TABLE (id INT);
	INSERT INTO @friendList SELECT * FROM DBO.fct_FriendList(@userID);
	SELECT * FROM DBO.Rater r WHERE r.id IN (SELECT id FROM @friendList)
END
GO

ALTER PROCEDURE DBO.sp_GetFriendRequestsReceived
	@userMail NVARCHAR(255)
AS
DECLARE @userID INT = DBO.fct_GetUserIdByMail(@userMail);
IF (@userID <> 0)
BEGIN
	SELECT 
	r.id, r.[name], r.firstname, r.username, r.mail
	FROM DBO.FriendRequest f
	INNER JOIN DBO.Rater r ON r.id = f.sentBy
	WHERE f.sentTo = @userID
END
GO

ALTER PROCEDURE DBO.sp_GetPendingFriendRequestsSent
	@userMail NVARCHAR(255)
AS
DECLARE @userID INT = DBO.fct_GetUserIdByMail(@userMail);
IF (@userID <> 0)
BEGIN
	SELECT 
	r.id, r.[name], r.firstname, r.username, r.mail
	FROM DBO.FriendRequest f
	INNER JOIN DBO.Rater r ON r.id = f.sentTo
	WHERE f.sentBy = @userID
END
GO

ALTER PROCEDURE DBO.sp_AddFriend
	@userMail	NVARCHAR(255),
	@friendID	INT
AS
DECLARE @return_value INT = 0;
DECLARE @userID INT = DBO.fct_GetUserIdByMail(@userMail);
IF (@userID = 0)
BEGIN
	SET @return_value = -1;	-- Requesting user not found
END
ELSE 
BEGIN
	IF NOT EXISTS (SELECT * FROM DBO.Rater r WHERE r.id = @friendID)
	BEGIN
		SET @return_value = -2;	-- Target user not found
	END
	ELSE IF (@userID = @friendID)
	BEGIN
		SET @return_value = -3;	-- Can't befriend yourself !
	END
	ELSE IF EXISTS (SELECT * FROM DBO.FriendList f WHERE 
						(f.user1 = @userID AND f.user2 = @friendID) OR
						(f.user1 = @friendID AND f.user2 = @userID))
	BEGIN
		SET @return_value = -4;	-- Already friends !	
	END
	ELSE
	BEGIN
		IF EXISTS (SELECT * FROM DBO.FriendRequest f WHERE
					(f.sentBy = @userID AND f.sentTo = @friendID))
		BEGIN
			SET @return_value = -5;	-- Friend request already sent
		END
		ELSE IF EXISTS (SELECT * FROM DBO.FriendRequest f WHERE
					(f.sentBy = @friendID AND f.sentTo = @userID))
		BEGIN
			UPDATE DBO.FriendRequest SET validated = 1 WHERE
			sentBy = @friendID AND sentTo = @userID;
			SET @return_value = 1;	-- Friend request accepted
		END
		ELSE
		BEGIN
			INSERT INTO DBO.FriendRequest VALUES
			(@friendID, @userID, 0);
			SET @return_value = 2;	-- Friend request sent
		END
	END
END
SELECT @return_value;
GO

ALTER PROCEDURE DBO.sp_RemoveFriend
	@userMail	NVARCHAR(255),
	@friendID	INT
AS
DECLARE @return_value INT = 0;
DECLARE @userID INT = DBO.fct_GetUserIdByMail(@userMail);
IF (@userID = 0)
BEGIN
	SET @return_value = -1;	-- Requesting user not found
END
ELSE 
BEGIN
	DELETE FROM DBO.FriendList WHERE
			(user1 = @userID AND user2 = @friendID) OR
			(user1 = @friendID AND user2 = @userID)
	
	DELETE FROM DBO.FriendRequest WHERE
			(sentTo = @userID AND sentBy = @friendID) OR
			(sentTo = @friendID AND sentBy = @userID)

	SET @return_value = 1;	-- Job's done
END
SELECT @return_value;
GO

ALTER PROCEDURE DBO.sp_SaveMedia
	@id				INT,
	@original_title	NVARCHAR(255),
	@title			NVARCHAR(255),
	@overview		NVARCHAR(255),
	@poster_path	NVARCHAR(255)

AS
IF NOT EXISTS (SELECT m.id FROM DBO.Media m WHERE m.id = @id)
BEGIN
	INSERT INTO DBO.Media VALUES (@id, @original_title, @title, @overview, @poster_path);
END
ELSE
BEGIN
	UPDATE DBO.Media SET
		original_title = @original_title,
		title = @title,
		overview = @overview,
		poster_path = @poster_path
	WHERE id = @id;
END
GO

ALTER PROCEDURE DBO.sp_SaveMedias
	@medias			TMDB_MEDIA READONLY
	-- NOT FUNCTIONAL
AS
INSERT INTO DBO.Media
	SELECT * FROM @medias tm
	WHERE tm.id NOT IN
	(SELECT m.id FROM DBO.Media m)
GO

ALTER PROCEDURE DBO.sp_GetMedia
	@id			INT
AS
SELECT
	m.*
FROM DBO.Media m
WHERE m.id = @id
GO

ALTER PROCEDURE DBO.sp_GetMediaPublicInfos
	@id				INT
AS
SELECT
	(SELECT DBO.fct_CalculateAverageGrade(m.id)) AS AverageScore,
	(SELECT DBO.fct_GetTotalSuggestions(m.id)) AS TotalSuggestionsByUsers,
	(SELECT DBO.fct_GetTotalSeen(m.id)) AS TotalSeenByUsers
FROM DBO.Media m
WHERE m.id = @id 
GO

ALTER PROCEDURE DBO.sp_GetMediaUserInfos
	@id				INT,
	@mail			NVARCHAR(255)
AS
DECLARE @userInfos AS TABLE (grade INT, comment TEXT);

IF (@mail IS NOT NULL)
BEGIN
	DECLARE @userID INT = DBO.fct_GetUserIdByMail(@mail);
	IF (@userID > 0)
	BEGIN
		INSERT INTO @userInfos
			SELECT
				rml.grade,
				rml.comment
			FROM DBO.RaterMediaList rml
			WHERE rml.media_id = @id AND rml.rater_id = @userID
	END
SELECT * FROM @userInfos;
END

SELECT * FROM @userInfos
GO

ALTER PROCEDURE DBO.sp_GetMediaComments
	@id			INT,
	@mail		NVARCHAR(255)
AS
DECLARE @userID INT = DBO.fct_GetUserIdByMail(@mail);
IF (@userID > 0)
BEGIN
	SELECT rml.comment as Comment_Value, 
	r.id, r.name, r.firstname, r.username, r.mail
	FROM DBO.Rater r
	INNER JOIN DBO.RaterMediaList rml
	ON r.id = rml.rater_id
	WHERE rml.media_id = @id AND
		  rml.comment IS NOT NULL AND 
		  rml.rater_id IN (SELECT * FROM DBO.fct_FriendList (@userID));
END
GO

ALTER PROCEDURE DBO.sp_GetMediaFriendsWhoWantToWatchIt
	@id			INT,
	@mail		NVARCHAR(255)
AS
DECLARE @userID INT = DBO.fct_GetUserIdByMail(@mail);
IF (@userID > 0)
BEGIN
	SELECT r.*
	FROM DBO.Rater r
	INNER JOIN DBO.RaterMediaList rml
	ON r.id = rml.rater_id
	WHERE rml.media_id = @id AND
		  rml.toWatch = 1 AND 
		  rml.rater_id IN (SELECT * FROM DBO.fct_FriendList (@userID));
END
GO

ALTER PROCEDURE DBO.sp_GetMediaFriendsWhoSawIt
	@id			INT,
	@mail		NVARCHAR(255)
AS
DECLARE @userID INT = DBO.fct_GetUserIdByMail(@mail);
IF (@userID > 0)
BEGIN
	SELECT r.*
	FROM DBO.Rater r
	INNER JOIN DBO.RaterMediaList rml
	ON r.id = rml.rater_id
	WHERE rml.media_id = @id AND
		  rml.seen = 1 AND 
		  rml.rater_id IN (SELECT * FROM DBO.fct_FriendList (@userID));
END
GO

CREATE PROCEDURE DBO.sp_GetFriendsWhoSuggestedMediaToMe
	@id			INT,
	@mail		NVARCHAR(255)
AS
DECLARE @userID INT = DBO.fct_GetUserIdByMail(@mail);
IF (@userID > 0)
BEGIN
	SELECT r.*
	FROM DBO.Rater r
	INNER JOIN DBO.MediaSuggestion ms
	ON r.id = ms.sender_id
	WHERE ms.media_id = @id AND
		  ms.receiver_id = @userID 
		  -- AND rml.rater_id IN (SELECT * FROM DBO.fct_FriendList (@userID));
END
GO

CREATE PROCEDURE DBO.sp_GetFriendsISuggestedThisMediaTo
	@id			INT,
	@mail		NVARCHAR(255)
AS
DECLARE @userID INT = DBO.fct_GetUserIdByMail(@mail);
IF (@userID > 0)
BEGIN
	SELECT r.*
	FROM DBO.Rater r
	INNER JOIN DBO.MediaSuggestion ms
	ON r.id = ms.receiver_id
	WHERE ms.media_id = @id AND
		  ms.sender_id = @userID 
		  -- AND rml.rater_id IN (SELECT * FROM DBO.fct_FriendList (@userID));
END
GO


ALTER PROCEDURE DBO.sp_GetWatchListByMail
	@mail		NVARCHAR(255)
AS
DECLARE @userID INT = DBO.fct_GetUserIdByMail(@mail);
IF (@userID > 0)
BEGIN
	SELECT rml.media_id FROM DBO.RaterMediaList rml WHERE rml.rater_id = @userID AND rml.toWatch = 1;
END
GO

ALTER PROCEDURE DBO.sp_GetWatchListById
	@id		INT
AS
SELECT rml.media_id FROM DBO.RaterMediaList rml WHERE rml.rater_id = @id AND rml.toWatch = 1;
GO

ALTER PROCEDURE DBO.sp_MarkMediaAsToWatch
	@userMail		NVARCHAR(255),
	@id				INT
AS
DECLARE @return_value INT = 0;
DECLARE @userID INT = DBO.fct_GetUserIdByMail(@userMail);
IF (@userID = 0)
BEGIN
	SET @return_value = -1;	-- Requesting user not found
END
ELSE 
BEGIN
	IF EXISTS (SELECT rml.* FROM DBO.RaterMediaList rml WHERE rml.rater_id = @userID AND rml.media_id = @id)
	BEGIN
		DECLARE @toWatch INT;
		SET @toWatch = (SELECT rml.toWatch FROM DBO.RaterMediaList rml WHERE rml.rater_id = @userID AND rml.media_id = @id);
		IF (@toWatch = 0)
		BEGIN
			UPDATE DBO.RaterMediaList SET toWatch = 1, seen = 0 WHERE rater_id = @userID AND media_id = @id;
			SET @return_value = 1	-- Media added to WatchList
		END
		ELSE
		BEGIN
			UPDATE DBO.RaterMediaList SET toWatch = 0 WHERE rater_id = @userID AND media_id = @id;
			SET @return_value = 2	-- Media removed from WatchList
		END
	END
	ELSE
	BEGIN
		INSERT INTO DBO.RaterMediaList VALUES (@userID, @id, 1, 0, null, null);
		SET @return_value = 1;		-- Media added to WatchList
	END
END
SELECT @return_value;
GO

ALTER PROCEDURE DBO.sp_GetSeenListByMail
	@mail		NVARCHAR(255)
AS
DECLARE @userID INT = DBO.fct_GetUserIdByMail(@mail);
IF (@userID > 0)
BEGIN
	SELECT rml.media_id FROM DBO.RaterMediaList rml WHERE rml.rater_id = @userID AND rml.seen = 1;
END
GO

ALTER PROCEDURE DBO.sp_GetSeenListById
	@id		INT
AS
SELECT rml.media_id FROM DBO.RaterMediaList rml WHERE rml.rater_id = @id AND rml.seen = 1;
GO

ALTER PROCEDURE DBO.sp_MarkMediaAsSeen
	@userMail		NVARCHAR(255),
	@id				INT
AS
DECLARE @return_value INT = 0;
DECLARE @userID INT = DBO.fct_GetUserIdByMail(@userMail);
IF (@userID = 0)
BEGIN
	SET @return_value = -1;	-- Requesting user not found
END
ELSE 
BEGIN
	IF EXISTS (SELECT rml.* FROM DBO.RaterMediaList rml WHERE rml.rater_id = @userID AND rml.media_id = @id)
	BEGIN
		DECLARE @seen INT;
		SET @seen = (SELECT rml.seen FROM DBO.RaterMediaList rml WHERE rml.rater_id = @userID AND rml.media_id = @id);
		IF (@seen = 0)
		BEGIN
			UPDATE DBO.RaterMediaList SET seen = 1, toWatch = 0 WHERE rater_id = @userID AND media_id = @id;
			SET @return_value = 1	-- Media added to seen list
		END
		ELSE
		BEGIN
			UPDATE DBO.RaterMediaList SET seen = 0 WHERE rater_id = @userID AND media_id = @id;
			SET @return_value = 2	-- Media removed from seen list
		END
	END
	ELSE
	BEGIN
		INSERT INTO DBO.RaterMediaList VALUES (@userID, @id, 0, 1, null, null);
		SET @return_value = 1;		-- Media added to seen list
	END
END
SELECT @return_value;
GO

ALTER PROCEDURE DBO.sp_CommentMedia
	@id			INT,
	@mail		NVARCHAR(255),
	@comment	TEXT
AS
DECLARE @return_value INT = 0;
DECLARE @userID INT = DBO.fct_GetUserIdByMail(@mail);
IF (@userID = 0)
BEGIN
	SET @return_value = -1;	-- Requesting user not found
END
ELSE
BEGIN
	IF EXISTS (SELECT rml.comment FROM DBO.RaterMediaList rml WHERE rml.media_id = @id AND rml.rater_id = @userID)
	BEGIN
		UPDATE DBO.RaterMediaList SET comment = @comment
		WHERE rater_id = @userID AND media_id = @id;
		SET @return_value = 1;	-- Updated comment !
	END
	ELSE
	BEGIN
		INSERT INTO DBO.RaterMediaList VALUES
		(@userID, @id, 0, 1, @comment, null)
		SET @return_value = 2;	-- Comment saved !
	END
END
SELECT @return_value;
GO

ALTER PROCEDURE DBO.sp_RateMedia
	@id			INT,
	@mail		NVARCHAR(255),
	@grade		INT
AS
DECLARE @return_value INT = 0;
DECLARE @userID INT = DBO.fct_GetUserIdByMail(@mail);
IF (@userID = 0)
BEGIN
	SET @return_value = -1;	-- Requesting user not found
END
ELSE
BEGIN
	IF EXISTS (SELECT rml.grade FROM DBO.RaterMediaList rml WHERE rml.media_id = @id AND rml.rater_id = @userID)
	BEGIN
		UPDATE DBO.RaterMediaList SET grade = @grade
		WHERE rater_id = @userID AND media_id = @id;
		SET @return_value = 1;	-- Updated grade !
	END
	ELSE
	BEGIN
		INSERT INTO DBO.RaterMediaList VALUES
		(@userID, @id, 0, 1, null, @grade)
		SET @return_value = 2;	-- Comment saved !
	END
END
SELECT @return_value;
GO	

--ALTER PROCEDURE DBO.sp_SuggestMedia
--	@id			INT,
--	@mail		NVARCHAR(255),
--	@target		INT
--AS
--DECLARE @return_value INT = 0;
--DECLARE @userID INT = DBO.fct_GetUserIdByMail(@mail);
--IF (@userID = 0)
--BEGIN
--	SET @return_value = -1;	-- Requesting user not found
--END
--ELSE IF NOT EXISTS (SELECT * FROM DBO.Rater r WHERE r.id = @target)
--BEGIN
--	SET @return_value = -2;	-- Target user not found
--END
--ELSE IF (@userID = @target)
--BEGIN
--	SET @return_value = -3;	-- Can't suggest a Media to yourself
--END
--ELSE
--BEGIN

--END
--SELECT @return_value;
--GO
