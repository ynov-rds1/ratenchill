﻿EXEC DBO.sp_AddFriend N'sand.dossantos@live.fr', 2		-- Utilisateur 1 demande l'utilisateur 2 en ami
EXEC DBO.sp_AddFriend N'sand.tests01@outlook.fr', 1		-- Utilisateur 2 accepte la requête de l'utilisateur 1


EXEC DBO.sp_AddFriend N'sand.tests01@outlook.fr', 3		-- Utilisateur 2 demande l'utilisateur 3 en ami
EXEC DBO.sp_AddFriend N'rdstestsmtp@gmail.com', 2		-- Utilisateur 3 accepte la requête de l'utilisateur 2


EXEC DBO.sp_AddFriend N'rdstestsmtp@gmail.com', 1		-- Utilisateur 3 demande l'utilisateur 1 en ami
EXEC DBO.sp_AddFriend N'sand.dossantos@live.fr', 3		-- Utilisateur 1 accepte la requête de l'utilisateur 3

EXEC DBO.sp_MarkMediaAsToWatch N'sand.tests01@outlook.fr', 122

EXEC DBO.sp_CommentMedia 121, N'sand.tests01@outlook.fr', 'Le meilleur des trois peut-être'
EXEC DBO.sp_RateMedia 121, N'sand.tests01@outlook.fr', 4

EXEC DBO.sp_CommentMedia 121, N'rdstestsmtp@gmail.com', 'Toujours magnifique en 2021'
EXEC DBO.sp_RateMedia 121, N'rdstestsmtp@gmail.com', 6



--------------------------------

SELECT * FROM DBO.Rater
SELECT * FROM DBO.FriendList
SELECT * FROM DBO.FriendRequest

DELETE FROM DBO.FriendList
DELETE FROM DBO.FriendRequest

EXEC DBO.sp_GetFriendList N'sand.dossantos@live.fr'
EXEC DBO.sp_GetFriendList N'rdstestsmtp@gmail.com'
EXEC DBO.sp_GetFriendList N'user@example.com'

EXEC DBO.sp_GetFriendRequestsReceived N'user@example.com'
EXEC DBO.sp_GetPendingFriendRequestsSent N'sand.dossantos@live.fr'


SELECT * FROM DBO.Media
SELECT * FROM DBO.RaterMediaList

DELETE FROM DBO.Media



Select DBO.fct_CalculateAverageGrade(121);
SELECT * FROM DBO.RaterMediaList WHERE media_id = 121